import * as env from './environment.constants';

export const environment = {
    production: true,
    apiUrl: env.constants.apiUrl.prod,
    cookieDomain: env.constants.cookieDomain.prod
};