export const constants = {
    apiUrl: {
        localhost: 'http://localhost:3333',
        dev: 'https://insetimax-back-end.herokuapp.com',
        prod: 'https://insetimax-back-end.herokuapp.com'
    },
    cookieDomain: {
        localhost: 'localhost',
        dev: '.vercel.app',
        prod: 'localhost'
    }
};