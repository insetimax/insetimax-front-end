import * as envConst from './environment.constants';

// let env: string = "localhost";
let env: string = "dev";
// let env: string = "prod";

export const environment = {
    production: false,
    apiUrl: envConst.constants.apiUrl[env],
    cookieDomain: envConst.constants.cookieDomain[env]
};
