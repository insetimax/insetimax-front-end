import * as env from './environment.constants';

export const environment = {
    production: false,
    apiUrl: env.constants.apiUrl.dev,
    cookieDomain: env.constants.cookieDomain.dev
};