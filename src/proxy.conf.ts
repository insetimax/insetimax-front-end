const PROXY_CONFIG = [
    {
        context: ['/apiUrl/localhost'],
        target: 'http://localhost:3333',
        secure: false,
        logLevel: 'debug',
        pathRewrite: { '^/apiUrl/localhost': '' }
    },
    {
        context: ['/apiUrl/dev'],
        target: 'https://insetimax-back-end.herokuapp.com/',
        secure: false,
        logLevel: 'debug',
        pathRewrite: { '^/apiUrl/dev': '' }
    },
    {
        context: ['/apiUrl/prod'],
        target: 'https://insetimax-back-end.herokuapp.com',
        secure: false,
        logLevel: 'debug',
        pathRewrite: { '^/apiUrl/prod': '' }
    }
];

module.exports = PROXY_CONFIG;