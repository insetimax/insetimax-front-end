export enum SidebarEnum {
    DASHBOARD = 'dashboard',
    REGISTER = 'register',
    REPORT = 'report'
}