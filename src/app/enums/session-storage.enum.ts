export enum SessionStorageEnum {
    LOGGED = 'logged',
    USER = 'user',
    SIDEBAR_MENU = 'sidebarMenu',
    CHANGING_PASSWORD = 'changingPassword'
}