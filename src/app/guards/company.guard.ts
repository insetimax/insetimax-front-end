import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { GenericService } from '../services/generic/generic.service';

@Injectable({
    providedIn: 'root'
})
export class CompanyGuard implements CanActivate {

    constructor(
        private router: Router,
        private generic: GenericService
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {
        let userAccess = this.generic.getUserAccess();

        if (userAccess?.id_company != null //
            && !!userAccess?.role?.is_admin //
        ) {
            return true;
        }

        this.router.navigate([`/dashboard`]);
        return false;
    }

}