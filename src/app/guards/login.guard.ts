import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { SessionStorageEnum } from '../enums/session-storage.enum';
import { LoginService } from '../services/login/login.service';

@Injectable({
    providedIn: 'root'
})
export class LoginGuard implements CanActivate {

    constructor(
        private sessionStorage: SessionStorageService,
        private loginService: LoginService
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {
        if (this.loginService.validateCookiesAndToken() //
            && this.sessionStorage.retrieve(SessionStorageEnum.LOGGED) //
        ) {
            return true;
        } else {
            this.loginService.logout();
            return false;
        }
    }

}