import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { SessionStorageService } from 'ngx-webstorage';
import { SessionStorageEnum } from './enums/session-storage.enum';
import { GenericService } from './services/generic/generic.service';
import { LoginService } from './services/login/login.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

    public logged: boolean = false;
    public changingPassword: boolean = false;

    public user;
    public userAccess;

    public sidebarMenu: string = 'dashboard';
    public mobileQuery: MediaQueryList;
    public infoFooter: string = '';

    public fillerNav = [{
        id: 'dashboard',
        route: 'dashboard',
        name: 'Dashboard',
        icon: 'dashboard'
    }, {
        id: 'register',
        route: 'register',
        name: 'Cadastros',
        icon: 'line_style'
    }, {
        id: 'report',
        route: 'report',
        name: 'Relatórios',
        icon: 'assignment'
    }];

    private _mobileQueryListener: () => void;

    constructor(
        private generic: GenericService,
        private loginService: LoginService,
        private sessionStorage: SessionStorageService,
        private changeDetectorRef: ChangeDetectorRef,
        private media: MediaMatcher
    ) {
        this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = () => this.changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);
    }

    ngOnInit(): void {
        this.logged = this.sessionStorage.retrieve(SessionStorageEnum.LOGGED);
        this.changingPassword = this.sessionStorage.retrieve(SessionStorageEnum.CHANGING_PASSWORD);

        this.user = this.generic.getUserFull();
        this.userAccess = this.generic.getUserAccess();
        this.setInfoFooter();

        this.localSubscriptions();
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this._mobileQueryListener);
    }

    localSubscriptions() {
        this.sessionStorage.observe(SessionStorageEnum.LOGGED).subscribe(result => {
            this.logged = result;
        });

        this.sessionStorage.observe(SessionStorageEnum.CHANGING_PASSWORD).subscribe(result => {
            this.changingPassword = result;
        });

        this.sessionStorage.observe(SessionStorageEnum.USER).subscribe(result => {
            this.user = result;
            this.setInfoFooter();
        });

        this.sessionStorage.observe(SessionStorageEnum.SIDEBAR_MENU).subscribe(result => {
            this.sidebarMenu = result;
        });
    }

    showCleanPage() {
        return !this.logged || !!this.changingPassword;
    }

    setInfoFooter() {
        if (this.userAccess[0]?.id_company != null)
            this.infoFooter = `${this.userAccess.company.name} | ${this.user.user.name}`;
        else if (this.user?.userAccess[0]?.id_owner != null)
            this.infoFooter = `${this.userAccess.owner.name} | ${this.user.user.name}`;
    }

    goToPage(page) {
        this.generic.goToPage(page);
    }

    logout() {
        this.loginService.logout();
    }

}