import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyGuard } from './guards/company.guard';
import { LoginGuard } from './guards/login.guard';
import { OwnerGuard } from './guards/owner.guard';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
import { ActivityCategoryFormComponent } from './pages/register/activity-category/activity-category-form/activity-category-form.component';
import { ActivityCategoryComponent } from './pages/register/activity-category/activity-category.component';
import { ChangePasswordComponent } from './pages/register/change-password/change-password.component';
import { CompanyFormComponent } from './pages/register/company/company-form/company-form.component';
import { CompanyComponent } from './pages/register/company/company.component';
import { PlaceActCategoryProductFormComponent } from './pages/register/place-act-category-product/place-act-category-product-form/place-act-category-product-form.component';
import { PlaceActCategoryProductComponent } from './pages/register/place-act-category-product/place-act-category-product.component';
import { PlaceActCategoryFormComponent } from './pages/register/place-act-category/place-act-category-form/place-act-category-form.component';
import { PlaceActCategoryComponent } from './pages/register/place-act-category/place-act-category.component';
import { PlaceFormComponent } from './pages/register/place/place-form/place-form.component';
import { PlaceComponent } from './pages/register/place/place.component';
import { ProductFormComponent } from './pages/register/product/product-form/product-form.component';
import { ProductComponent } from './pages/register/product/product.component';
import { RegisterComponent } from './pages/register/register.component';
import { UserAccessFormComponent } from './pages/register/user-access/user-access-form/user-access-form.component';
import { UserAccessComponent } from './pages/register/user-access/user-access.component';
import { UserFormComponent } from './pages/register/user/user-form/user-form.component';
import { UserComponent } from './pages/register/user/user.component';
import { CleaningreportComponent } from './pages/report/cleaning-report/cleaning-report.component';
import { ReportComponent } from './pages/report/report.component';

const routes: Routes = [
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    //
    // ************************************************************************************************************************************************************************************
    // DASHBOARD routes
    // ************************************************************************************************************************************************************************************
    //
    { path: 'dashboard', component: DashboardComponent, canActivate: [LoginGuard] },
    { path: 'dashboard/:byLogin', component: DashboardComponent, canActivate: [LoginGuard] },
    //
    // ************************************************************************************************************************************************************************************
    // REGISTER routes
    // ************************************************************************************************************************************************************************************
    //
    { path: 'register', component: RegisterComponent, canActivate: [LoginGuard] },
    //
    { path: 'activityCategory', component: ActivityCategoryComponent, canActivate: [LoginGuard, OwnerGuard] },
    { path: 'activityCategoryForm', component: ActivityCategoryFormComponent, canActivate: [LoginGuard, OwnerGuard] },
    { path: 'activityCategoryForm/:id', component: ActivityCategoryFormComponent, canActivate: [LoginGuard, OwnerGuard] },
    //
    { path: 'product', component: ProductComponent, canActivate: [LoginGuard, OwnerGuard] },
    { path: 'productForm', component: ProductFormComponent, canActivate: [LoginGuard, OwnerGuard] },
    { path: 'productForm/:id', component: ProductFormComponent, canActivate: [LoginGuard, OwnerGuard] },
    //
    { path: 'company', component: CompanyComponent, canActivate: [LoginGuard] },
    { path: 'companyForm', component: CompanyFormComponent, canActivate: [LoginGuard] },
    { path: 'companyForm/:id', component: CompanyFormComponent, canActivate: [LoginGuard] },
    //
    { path: 'place/:idCompany', component: PlaceComponent, canActivate: [LoginGuard, CompanyGuard] },
    { path: 'placeForm/:idCompany', component: PlaceFormComponent, canActivate: [LoginGuard, CompanyGuard] },
    { path: 'placeForm/:idCompany/:id', component: PlaceFormComponent, canActivate: [LoginGuard, CompanyGuard] },
    //
    { path: 'placeActCategory/:idPlace', component: PlaceActCategoryComponent, canActivate: [LoginGuard, CompanyGuard] },
    { path: 'placeActCategoryForm/:idPlace', component: PlaceActCategoryFormComponent, canActivate: [LoginGuard, CompanyGuard] },
    { path: 'placeActCategoryForm/:idPlace/:id', component: PlaceActCategoryFormComponent, canActivate: [LoginGuard, CompanyGuard] },
    //
    { path: 'placeActCategoryProduct/:idPlaceActCategory', component: PlaceActCategoryProductComponent, canActivate: [LoginGuard, CompanyGuard] },
    { path: 'placeActCategoryProductForm/:idPlaceActCategory', component: PlaceActCategoryProductFormComponent, canActivate: [LoginGuard, CompanyGuard] },
    { path: 'placeActCategoryProductForm/:idPlaceActCategory/:id', component: PlaceActCategoryProductFormComponent, canActivate: [LoginGuard, CompanyGuard] },
    //
    { path: 'user', component: UserComponent, canActivate: [LoginGuard] },
    { path: 'userForm/:id', component: UserFormComponent, canActivate: [LoginGuard] },
    //
    { path: 'userAccess/byOwner/:idOwner', component: UserAccessComponent, canActivate: [LoginGuard, OwnerGuard] },
    { path: 'userAccessForm/byOwner/:idOwner', component: UserAccessFormComponent, canActivate: [LoginGuard, OwnerGuard] },
    { path: 'userAccessForm/byOwner/:idOwner/:id', component: UserAccessFormComponent, canActivate: [LoginGuard, OwnerGuard] },
    //
    { path: 'userAccess/byCompany/:idCompany', component: UserAccessComponent, canActivate: [LoginGuard] },
    { path: 'userAccessForm/byCompany/:idCompany', component: UserAccessFormComponent, canActivate: [LoginGuard] },
    { path: 'userAccessForm/byCompany/:idCompany/:id', component: UserAccessFormComponent, canActivate: [LoginGuard] },
    //
    { path: 'changePassword/:idUser', component: ChangePasswordComponent },
    //
    // ************************************************************************************************************************************************************************************
    // REPORT routes
    // ************************************************************************************************************************************************************************************
    //
    { path: 'report', component: ReportComponent, canActivate: [LoginGuard] },
    { path: 'cleaningReport', component: CleaningreportComponent, canActivate: [LoginGuard] },
    //
    // ************************************************************************************************************************************************************************************
    // OTHER routes
    // ************************************************************************************************************************************************************************************
    //
    { path: 'login', component: LoginComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
