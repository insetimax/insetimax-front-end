import { Injectable } from '@angular/core';
import { HttpMethodEnum } from 'src/app/enums/http-method.enum';
import { HttpClientService } from 'src/app/services/http-client/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class ActivityCategoryService {

    constructor(
        private httpClientService: HttpClientService
    ) {
    }

    getById(id) {
        return this.httpClientService.request(HttpMethodEnum.GET, `activityCategory/${id}`, null, null, true);
    }

    getByOwner(idOwner) {
        return this.httpClientService.request(HttpMethodEnum.GET, `activityCategory/byOwner/${idOwner}`, null, null, true);
    }

    post(body) {
        return this.httpClientService.request(HttpMethodEnum.POST, 'activityCategory', body, null, true);
    }

    put(id, body) {
        return this.httpClientService.request(HttpMethodEnum.PUT, `activityCategory/${id}`, body, null, true);
    }

    delete(id) {
        return this.httpClientService.request(HttpMethodEnum.DELETE, `activityCategory/${id}`, null, null, true);
    }

}
