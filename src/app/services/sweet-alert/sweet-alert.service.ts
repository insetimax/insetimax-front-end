import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class SweetAlertService {

    // private cancelButtonColor: string = '#d33';
    // private confirmButtonColor: string = '#DD6B55';

    private messageBtnConfirm?: string;
    private messageBtnCancel?: string;

    constructor(
    ) {
    }

    ok(retSweet: any) {
        return (!!retSweet['value'] && (!retSweet['dismiss'] || retSweet['dismiss'] !== this.getCancelReason()))
    }

    getCancelReason() {
        return Swal.DismissReason.cancel;
    }

    // type: success, error, warning, info, question
    alertConfirm(title: string, text: string, confirmButtonText?: string, cancelButtonText?: string) {
        this.messageBtnConfirm = 'Sim';
        this.messageBtnCancel = 'Cancelar';

        return Swal.fire({
            title: title,
            html: text,
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: confirmButtonText || this.messageBtnConfirm,
            cancelButtonText: cancelButtonText || this.messageBtnCancel,
            // cancelButtonColor: this.cancelButtonColor,
            // confirmButtonColor: this.confirmButtonColor,
            allowOutsideClick: false
        });
    }

}
