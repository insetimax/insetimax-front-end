import { Injectable } from '@angular/core';
import { HttpMethodEnum } from 'src/app/enums/http-method.enum';
import { HttpClientService } from 'src/app/services/http-client/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(
        private httpClientService: HttpClientService
    ) {
    }

    getById(id) {
        return this.httpClientService.request(HttpMethodEnum.GET, `user/byId/${id}`, null, null, true);
    }

    getByCpf(cpf) {
        return this.httpClientService.request(HttpMethodEnum.GET, `user/byCpf/${cpf}`, null, null, true);
    }

    getAllUsersOfCompany(idUser) {
        return this.httpClientService.request(HttpMethodEnum.GET, `user/getAllUsersOfCompany/${idUser}`, null, null, true);
    }

    getAllUsersOfOwner(idUser) {
        return this.httpClientService.request(HttpMethodEnum.GET, `user/getAllUsersOfOwner/${idUser}`, null, null, true);
    }

    post(body) {
        return this.httpClientService.request(HttpMethodEnum.POST, 'user', body, null, true);
    }

    put(id, body) {
        return this.httpClientService.request(HttpMethodEnum.PUT, `user/${id}`, body, null, true);
    }

    delete(id) {
        return this.httpClientService.request(HttpMethodEnum.DELETE, `user/${id}`, null, null, true);
    }

    // Change password

    getByIdChangePassword(id) {
        return this.httpClientService.request(HttpMethodEnum.GET, `user/byIdChangePassword/${id}`, null, null, false);
    }

    changePassword(id, body) {
        return this.httpClientService.request(HttpMethodEnum.PUT, `user/changePassword/${id}`, body, null, null, false);
    }

    forgotPassword(body) {
        return this.httpClientService.request(HttpMethodEnum.POST, `user/forgotPassword`, body, null, null, false);
    }

}
