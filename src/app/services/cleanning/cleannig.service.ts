import { Injectable } from '@angular/core';
import { HttpMethodEnum } from 'src/app/enums/http-method.enum';
import { HttpClientService } from 'src/app/services/http-client/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class CleanningService {

    constructor(
        private httpClientService: HttpClientService
    ) {
    }

    cleaningReport(filters) {
        let params = filters.idOwner;

        if (filters.idCompanyParent)
            params += `/${filters.idCompanyParent}`;
        else
            params += '/-1';

        if (filters.idUser)
            params += `/${filters.idUser}`;
        else
            params += '/-1';

        if (filters.idCompany)
            params += `/${filters.idCompany}`;
        else
            params += '/-1';

        if (filters.idPlace)
            params += `/${filters.idPlace}`;
        else
            params += '/-1';

        if (filters.idActCategory)
            params += `/${filters.idActCategory}`;
        else
            params += '/-1';

        if (filters.startTime && filters.endTime)
            params += `/${filters.startTime}/${filters.endTime}`;
        else
            params += '/-1/-1';

        if (filters.limit)
            params += `/${filters.limit}`;
        else
            params += '/-1';

        return this.httpClientService.request(HttpMethodEnum.GET, `cleaning/cleaningReport/${params}`, null, null, true);
    }

}