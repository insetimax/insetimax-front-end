import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SessionStorageService } from 'ngx-webstorage';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';

@Injectable({
    providedIn: 'root'
})
export class GenericService {

    public milliSecondsInDay = 86400000; //number of milliseconds in a day

    constructor(
        public sessionStorage: SessionStorageService,
        public toastr: ToastrService,
        public router: Router
    ) {
    }

    getUserFull() {
        return this.sessionStorage.retrieve(SessionStorageEnum.USER);
    }

    getUser() {
        let user = this.sessionStorage.retrieve(SessionStorageEnum.USER);
        return user.user;
    }

    getUserAccess() {
        let user = this.sessionStorage.retrieve(SessionStorageEnum.USER);
        return user.userAccess[0];
    }

    getToken() {
        let user = this.sessionStorage.retrieve(SessionStorageEnum.USER);
        return user.token;
    }

    goToPage(page) {
        this.router.navigate([`/${page}`]);
    }

}