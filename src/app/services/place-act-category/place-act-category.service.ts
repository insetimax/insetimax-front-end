import { Injectable } from '@angular/core';
import { HttpMethodEnum } from 'src/app/enums/http-method.enum';
import { HttpClientService } from 'src/app/services/http-client/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class PlaceActCategoryService {

    constructor(
        private httpClientService: HttpClientService
    ) {
    }

    getById(id) {
        return this.httpClientService.request(HttpMethodEnum.GET, `placeActCategory/${id}`, null, null, true);
    }

    getByPlace(idPlace) {
        return this.httpClientService.request(HttpMethodEnum.GET, `placeActCategory/byPlace/${idPlace}`, null, null, true);
    }

    post(body) {
        return this.httpClientService.request(HttpMethodEnum.POST, 'placeActCategory', body, null, true);
    }

    put(id, body) {
        return this.httpClientService.request(HttpMethodEnum.PUT, `placeActCategory/${id}`, body, null, true);
    }

    delete(id) {
        return this.httpClientService.request(HttpMethodEnum.DELETE, `placeActCategory/${id}`, null, null, true);
    }

}
