import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { SessionStorageService } from 'ngx-webstorage';
import { HttpMethodEnum } from 'src/app/enums/http-method.enum';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { environment } from 'src/environments/environment';
import { HttpClientService } from '../../services/http-client/http-client.service';
import jwt_decode from 'jwt-decode';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    constructor(
        private sessionStorage: SessionStorageService,
        private httpClientService: HttpClientService,
        private cookieService: CookieService,
        private toastr: ToastrService,
        public router: Router
    ) {
    }

    public logout() {
        this.sessionStorage.clear();
        this.cookieService.deleteAll('/', environment.cookieDomain);
        this.router.navigate(['/login']);
    }

    public async validateLogin(cpf, password) {
        let body = {
            cpf: cpf,
            password: password
        }

        this.httpClientService.request(HttpMethodEnum.POST, `user/webAuthorization`, body, null, null, true).subscribe(async (result: any) => {
            this.setToken(result.token.token);
            this.setRefreshToken(result.token.refreshToken);

            if (await this.validateCookiesAndToken()) {
                this.sessionStorage.store(SessionStorageEnum.USER, result);
                this.sessionStorage.store(SessionStorageEnum.LOGGED, true);
                this.toastr.success('Login efetuado com sucesso', 'Login');
                if (!!result.user.updatepass) {
                    this.sessionStorage.store(SessionStorageEnum.CHANGING_PASSWORD, true);
                    this.router.navigateByUrl(`/changePassword/${result.user.id}`);
                } else {
                    this.router.navigateByUrl('/dashboard/true');
                }
            }
        }, error => {
            this.sessionStorage.store(SessionStorageEnum.LOGGED, false);
            this.sessionStorage.clear(SessionStorageEnum.USER);

            this.toastr.error('Usuário ou senha inválidos', 'Login');
            console.log(error);
        });
    }

    public async validateCookiesAndToken() {
        if (this.validateCookies() && await this.validateToken()) {
            return true;
        }
        return false;
    }

    private setToken(idToken) {
        let cookies = this.cookieService.getAll() || {};
        const idTokenKey = Object.keys(cookies).find(k => k.includes('idToken'));

        if (!!idTokenKey) {
            const yesterday = new Date(new Date().setDate(new Date().getDate() - 1));
            document.cookie = `${idTokenKey}=${idToken}; expires=${yesterday}; path=/`;

            const tomorrow = new Date(new Date().setDate(new Date().getDate() + 1));
            document.cookie = `${idTokenKey}=${idToken}; expires=${tomorrow}; path=/`;
        } else {
            const tomorrow = new Date(new Date().setDate(new Date().getDate() + 1));
            document.cookie = `idToken=${idToken}; expires=${tomorrow}; path=/`;
        }
    }

    private setRefreshToken(refreshToken) {
        document.cookie = `refreshToken=${refreshToken}; path=/`;
    }

    private validateCookies(): boolean {
        let hasCookies = true;
        let cookies = this.cookieService.getAll() || {};
        hasCookies = hasCookies && Object.keys(cookies).some(k => k.includes('idToken'));
        hasCookies = hasCookies && Object.keys(cookies).some(k => k.includes('refreshToken'));
        return hasCookies;
    }

    private async validateToken() {
        let cookies = this.cookieService.getAll() || {};
        let letIdToken = Object.keys(cookies).find(k => k.includes('idToken'));
        let letRefreshToken = Object.keys(cookies).find(k => k.includes('refreshToken'));
        const idToken = letIdToken ? cookies[letIdToken] : null;
        const refreshToken = letRefreshToken ? cookies[letRefreshToken] : null;

        if (!this.tokenIsOk(idToken)) {
            const refreshTokenBody = {
                refreshToken: refreshToken
            }

            const refreshTokenResult: any = await this.httpClientService.request(HttpMethodEnum.POST, `user/refreshToken`, refreshTokenBody, null, null, false).toPromise();
            if (!!refreshTokenResult) {
                this.setToken(refreshTokenResult.token);
                this.setRefreshToken(refreshTokenResult.refreshToken);
                return Promise.resolve(true);
            } else {
                return Promise.resolve(false);
            }
        } else {
            return Promise.resolve(true);
        }
    }

    private tokenIsOk(token: string) {
        const tokenInfo = this.getDecodedAccessToken(token);
        return new Date() < new Date(tokenInfo.data.expiresAt);
    }

    private getDecodedAccessToken(token: string): any {
        try {
            return jwt_decode(token);
        } catch (Error) {
            return null;
        }
    }

}