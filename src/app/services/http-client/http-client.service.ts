import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { environment } from 'src/environments/environment';

export type IHttpOptions = {
    headers?: { [header: string]: any; };
    params?: { [param: string]: any; };
}

@Injectable({
    providedIn: 'root'
})
export class HttpClientService {

    protected _httpOptions: IHttpOptions;

    constructor(
        private cookieService: CookieService,
        private httpClient: HttpClient,
        private toastr: ToastrService
    ) {
    }

    protected addHeader(key: string, value: any) {
        if (!this._httpOptions) this._httpOptions = {};
        this._httpOptions.headers = {
            ...this._httpOptions.headers,
            [key]: '' + value
        }
    }

    protected addParams(key: string, value: any) {
        if (!this._httpOptions) this._httpOptions = {};
        this._httpOptions.params = {
            ...this._httpOptions.params,
            [key]: '' + value
        }
    }

    protected mergeOptions(options: IHttpOptions = {}): IHttpOptions {
        let newOptions: IHttpOptions = {}

        newOptions.headers = { ...this._httpOptions?.headers };
        Object.entries(options?.headers || {}).forEach(([key, value]) => {
            if (!!newOptions.headers)
                newOptions.headers[key] = '' + value;
        });

        newOptions.params = { ...this._httpOptions?.params };
        Object.entries(options?.params || {}).forEach(([key, value]) => {
            if (!!newOptions.params)
                newOptions.params[key] = '' + value;
        });

        return newOptions;
    }

    request<R>(method: string, endpoint: string, body?: any, options?: IHttpOptions, setToken?: boolean, isLogin?: boolean) {
        if (!!setToken) {
            let cookies = this.cookieService.getAll() || {};
            let idTokenKey = Object.keys(cookies).find(k => k.includes('idToken'));
            this.addHeader('Authorization', `bearer ${cookies[idTokenKey]}`);
        }

        let requestOptions: IHttpOptions & { body?: any } = this.mergeOptions(options);
        requestOptions.body = body || {};

        return this.httpClient.request(method, `${environment.apiUrl}/${endpoint}`, requestOptions).pipe(
            catchError(error => {
                if (!isLogin) {
                    if (!!error?.error?.message) {
                        this.toastr.error(error?.error?.message, 'Ops! Ocorreu um erro no servidor');
                    } else if (!!error?.error?.error?.message) {
                        this.toastr.error(error?.error?.error?.message, 'Ops! Ocorreu um erro no servidor');
                    } else {
                        this.toastr.error(error?.message, 'Ops! Ocorreu um erro no servidor');
                    }
                }
                return throwError(error);
            })
        );
    }

}