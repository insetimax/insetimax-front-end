import { Injectable } from '@angular/core';
import { HttpMethodEnum } from 'src/app/enums/http-method.enum';
import { HttpClientService } from 'src/app/services/http-client/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class RoleService {

    constructor(
        private httpClientService: HttpClientService
    ) {
    }

    get() {
        return this.httpClientService.request(HttpMethodEnum.GET, `role`, null, null, true);
    }

    getById(id) {
        return this.httpClientService.request(HttpMethodEnum.GET, `role/${id}`, null, null, true);
    }

}
