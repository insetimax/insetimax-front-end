import { Injectable } from '@angular/core';
import { HttpMethodEnum } from 'src/app/enums/http-method.enum';
import { HttpClientService } from 'src/app/services/http-client/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class UserAccessService {

    constructor(
        private httpClientService: HttpClientService
    ) {
    }

    getById(id) {
        return this.httpClientService.request(HttpMethodEnum.GET, `userAccess/${id}`, null, null, true);
    }

    getByOwner(idOwner) {
        return this.httpClientService.request(HttpMethodEnum.GET, `userAccess/byOwner/${idOwner}`, null, null, true);
    }

    getByCompany(idCompany) {
        return this.httpClientService.request(HttpMethodEnum.GET, `userAccess/byCompany/${idCompany}`, null, null, true);
    }

    getByCompanyParent(idCompany) {
        return this.httpClientService.request(HttpMethodEnum.GET, `userAccess/byCompanyParent/${idCompany}`, null, null, true);
    }

    getByCompanyByOwner(idOwner) {
        return this.httpClientService.request(HttpMethodEnum.GET, `userAccess/byCompanyByOwner/${idOwner}`, null, null, true);
    }

    post(body) {
        return this.httpClientService.request(HttpMethodEnum.POST, 'userAccess', body, null, true);
    }

    put(id, body) {
        return this.httpClientService.request(HttpMethodEnum.PUT, `userAccess/${id}`, body, null, true);
    }

    delete(id) {
        return this.httpClientService.request(HttpMethodEnum.DELETE, `userAccess/${id}`, null, null, true);
    }

}
