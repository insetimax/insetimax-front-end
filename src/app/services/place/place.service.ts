import { Injectable } from '@angular/core';
import { HttpMethodEnum } from 'src/app/enums/http-method.enum';
import { HttpClientService } from 'src/app/services/http-client/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class PlaceService {

    constructor(
        private httpClientService: HttpClientService
    ) {
    }

    getById(id) {
        return this.httpClientService.request(HttpMethodEnum.GET, `place/${id}`, null, null, true);
    }

    getByCompany(idCompany) {
        return this.httpClientService.request(HttpMethodEnum.GET, `place/byCompany/${idCompany}`, null, null, true);
    }

    post(body) {
        return this.httpClientService.request(HttpMethodEnum.POST, 'place', body, null, true);
    }

    put(id, body) {
        return this.httpClientService.request(HttpMethodEnum.PUT, `place/${id}`, body, null, true);
    }

    delete(id) {
        return this.httpClientService.request(HttpMethodEnum.DELETE, `place/${id}`, null, null, true);
    }

}
