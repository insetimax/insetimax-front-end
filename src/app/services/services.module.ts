import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { ActivityCategoryService } from "./activity-category/activity-category.service";
import { CleanningService } from "./cleanning/cleannig.service";
import { CompanyService } from "./company/company.service";
import { OwnerService } from "./company/owner.service";
import { GenericService } from "./generic/generic.service";
import { LoginService } from "./login/login.service";
import { PlaceActCategoryProductService } from "./place-act-category-product/place-act-category-product.service";
import { PlaceActCategoryService } from "./place-act-category/place-act-category.service";
import { PlaceService } from "./place/place.service";
import { ProductService } from "./product/product.service";
import { RoleService } from "./role/role.service";
import { SweetAlertService } from "./sweet-alert/sweet-alert.service";
import { UserAccessService } from "./user-access/user-access.service";
import { UserService } from "./user/user.service";

@NgModule({
    declarations: [
    ],
    imports: [
    ],
    providers: [
        LoginService,
        GenericService,
        SweetAlertService,
        ActivityCategoryService,
        ProductService,
        CompanyService,
        OwnerService,
        PlaceService,
        PlaceActCategoryService,
        PlaceActCategoryProductService,
        UserService,
        UserAccessService,
        RoleService,
        CleanningService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ServicesModule { }