import { Injectable } from '@angular/core';
import { HttpMethodEnum } from 'src/app/enums/http-method.enum';
import { HttpClientService } from 'src/app/services/http-client/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class CompanyService {

    constructor(
        private httpClientService: HttpClientService
    ) {
    }

    getById(id) {
        return this.httpClientService.request(HttpMethodEnum.GET, `company/${id}`, null, null, true);
    }

    getByOwner(idOwner) {
        return this.httpClientService.request(HttpMethodEnum.GET, `company/byOwner/${idOwner}`, null, null, true);
    }

    getByParent(idParent) {
        return this.httpClientService.request(HttpMethodEnum.GET, `company/byParent/${idParent}`, null, null, true);
    }

    post(body) {
        return this.httpClientService.request(HttpMethodEnum.POST, 'company', body, null, true);
    }

    put(id, body) {
        return this.httpClientService.request(HttpMethodEnum.PUT, `company/${id}`, body, null, true);
    }

    delete(id) {
        return this.httpClientService.request(HttpMethodEnum.DELETE, `company/${id}`, null, null, true);
    }

}
