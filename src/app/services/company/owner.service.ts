import { Injectable } from '@angular/core';
import { HttpMethodEnum } from 'src/app/enums/http-method.enum';
import { HttpClientService } from 'src/app/services/http-client/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class OwnerService {

    constructor(
        private httpClientService: HttpClientService
    ) {
    }

    getById(id) {
        return this.httpClientService.request(HttpMethodEnum.GET, `owner/${id}`, null, null, true);
    }

}
