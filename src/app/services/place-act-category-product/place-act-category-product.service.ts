import { Injectable } from '@angular/core';
import { HttpMethodEnum } from 'src/app/enums/http-method.enum';
import { HttpClientService } from 'src/app/services/http-client/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class PlaceActCategoryProductService {

    constructor(
        private httpClientService: HttpClientService
    ) {
    }

    getById(id) {
        return this.httpClientService.request(HttpMethodEnum.GET, `placeActCategoryProduct/${id}`, null, null, true);
    }

    getByPlaceActCategory(idPlaceActCategory) {
        return this.httpClientService.request(HttpMethodEnum.GET, `placeActCategoryProduct/byPlaceActCategory/${idPlaceActCategory}`, null, null, true);
    }

    post(body) {
        return this.httpClientService.request(HttpMethodEnum.POST, 'placeActCategoryProduct', body, null, true);
    }

    put(id, body) {
        return this.httpClientService.request(HttpMethodEnum.PUT, `placeActCategoryProduct/${id}`, body, null, true);
    }

    delete(id) {
        return this.httpClientService.request(HttpMethodEnum.DELETE, `placeActCategoryProduct/${id}`, null, null, true);
    }

}
