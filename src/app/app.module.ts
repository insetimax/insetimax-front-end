import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { LoadingBarHttpModule } from '@ngx-loading-bar/http';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { NgxMaskModule } from 'ngx-mask';
import { ToastrModule } from 'ngx-toastr';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageFooterComponent } from './components/page/page-footer/page-footer.component';
import { PageHeaderComponent } from './components/page/page-header/page-header.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { TableComponent } from './components/table/table.component';
import { CompanyGuard } from './guards/company.guard';
import { LoginGuard } from './guards/login.guard';
import { OwnerGuard } from './guards/owner.guard';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ForgotPasswordDialog, LoginComponent } from './pages/login/login.component';
import { ActivityCategoryFormComponent } from './pages/register/activity-category/activity-category-form/activity-category-form.component';
import { ActivityCategoryComponent } from './pages/register/activity-category/activity-category.component';
import { ChangePasswordComponent } from './pages/register/change-password/change-password.component';
import { CompanyFormComponent } from './pages/register/company/company-form/company-form.component';
import { CompanyComponent } from './pages/register/company/company.component';
import { PlaceActCategoryProductFormComponent } from './pages/register/place-act-category-product/place-act-category-product-form/place-act-category-product-form.component';
import { PlaceActCategoryProductComponent } from './pages/register/place-act-category-product/place-act-category-product.component';
import { PlaceActCategoryFormComponent } from './pages/register/place-act-category/place-act-category-form/place-act-category-form.component';
import { PlaceActCategoryComponent } from './pages/register/place-act-category/place-act-category.component';
import { PlaceFormComponent } from './pages/register/place/place-form/place-form.component';
import { PlaceComponent } from './pages/register/place/place.component';
import { ProductFormComponent } from './pages/register/product/product-form/product-form.component';
import { ProductComponent } from './pages/register/product/product.component';
import { RegisterComponent } from './pages/register/register.component';
import { UserAccessFormComponent } from './pages/register/user-access/user-access-form/user-access-form.component';
import { UserAccessComponent } from './pages/register/user-access/user-access.component';
import { UserFormComponent } from './pages/register/user/user-form/user-form.component';
import { UserComponent } from './pages/register/user/user.component';
import { CleaningreportComponent } from './pages/report/cleaning-report/cleaning-report.component';
import { ReportComponent } from './pages/report/report.component';
import { ServicesModule } from './services/services.module';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        DashboardComponent,
        RegisterComponent,
        PageHeaderComponent,
        PageFooterComponent,
        TableComponent,
        ActivityCategoryComponent,
        ActivityCategoryFormComponent,
        ProductComponent,
        ProductFormComponent,
        ProductFormComponent,
        CompanyComponent,
        CompanyFormComponent,
        PlaceComponent,
        PlaceFormComponent,
        PlaceActCategoryComponent,
        PlaceActCategoryFormComponent,
        PlaceActCategoryProductComponent,
        PlaceActCategoryProductFormComponent,
        UserComponent,
        UserAccessComponent,
        UserAccessFormComponent,
        UserFormComponent,
        ChangePasswordComponent,
        ForgotPasswordDialog,
        ReportComponent,
        CleaningreportComponent,
        ProgressBarComponent,
    ],
    imports: [
        CommonModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgxWebstorageModule.forRoot(),
        NgxMaskModule.forRoot(),
        ToastrModule.forRoot({
            timeOut: 10000,
            positionClass: 'toast-top-center',
            preventDuplicates: true,
        }),
        SweetAlert2Module.forRoot(),
        MatSidenavModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatMenuModule,
        MatTableModule,
        MatPaginatorModule,
        MatCheckboxModule,
        MatSelectModule,
        MatDividerModule,
        MatDialogModule,
        MatExpansionModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatGridListModule,
        //
        NgxQRCodeModule,
        //
        LoadingBarHttpClientModule,
        LoadingBarRouterModule,
        LoadingBarHttpModule,
        LoadingBarModule,
        //
        ServicesModule
    ],
    providers: [
        LoginGuard,
        OwnerGuard,
        CompanyGuard,
        { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' }
    ],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
