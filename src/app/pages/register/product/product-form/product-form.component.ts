import { Component, Injector } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { ProductService } from '../../../../services/product/product.service';
import { GenericFormPage } from '../../../generic/generic-form.page';

@Component({
    templateUrl: './product-form.component.html',
    styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent extends GenericFormPage {

    public imgFile: string;

    constructor(
        private sanitizer: DomSanitizer,
        private service: ProductService,
        public injector: Injector
    ) {
        super(injector);
    }

    onInit(): void {
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public pageTitle(): string {
        return 'Produto';
    }

    public pageMain(): string {
        return 'product';
    }

    public setForm(): void {
        this.form = new FormGroup({
            code: new FormControl('', [Validators.required]),
            name: new FormControl('', [Validators.required]),
            info: new FormControl('', [Validators.required]),
            active: new FormControl(true, [Validators.required]),
            image: new FormControl('')
        });
    }

    public getById(): void {
        this.service.getById(this.id).subscribe(result => {
            this.model = !!result ? result[0] : {};
            this.title += ` > ${this.model?.code} - ${this.model?.name}`;

            this.form.setValue({
                code: this.model.code,
                name: this.model.name,
                info: this.model.info,
                active: this.model.active,
                image: this.model.image
            });
        });
    }

    public save(): void {
        let body = {
            id_owner: this.userAccess.id_owner,
            code: this.form.get('code').value,
            name: this.form.get('name').value,
            info: this.form.get('info').value,
            image: this.form.get('image').value,
            active: this.form.get('active').value
        }

        if (!!this.isEdit) {
            this.service.put(this.id, body).subscribe(result => {
                this.generic.toastr.success(`Registro editado com sucesso`, this.pageTitle());
                this.generic.router.navigateByUrl(`/${this.pageMain()}`);
            });
        } else {
            this.service.post(body).subscribe(result => {
                this.generic.toastr.success(`Registro criada com sucesso`, this.pageTitle());
                this.generic.router.navigateByUrl(`/${this.pageMain()}`);
            });
        }
    }

    public patchImage(image: any): void {
        this.form.patchValue({
            'image': image
        });
    }

}
