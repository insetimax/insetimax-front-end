import { Component, Injector } from '@angular/core';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { GenericPage } from '../../generic/generic.page';
import { UserAccessService } from '../../../services/user-access/user-access.service';

@Component({
    templateUrl: './user-access.component.html',
    styleUrls: ['./user-access.component.scss']
})
export class UserAccessComponent extends GenericPage {

    constructor(
        private service: UserAccessService,
        public injector: Injector
    ) {
        super(injector);
    }

    public onInit(): void {
    }

    public pageTitle(): string {
        return 'Acessos de usuários';
    }

    public pageForm(): string {
        if (this.idCompany != null)
            return `userAccessForm/byCompany/${this.idCompany}`;
        else if (this.idOwner != null)
            return `userAccessForm/byOwner/${this.idOwner}`;
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public setColumns() {
        this.columns = [{
            matColumnDef: 'id',
            title: 'ID',
            attribute: 'id',
            isActive: false
        }, {
            matColumnDef: 'company',
            title: 'Empresa responsável',
            attribute: 'company_name',
            attributeElse: 'owner_name',
            isActive: false
        }, {
            matColumnDef: 'role',
            title: 'Função',
            attribute: 'role_name',
            isActive: false
        }, {
            matColumnDef: 'user',
            title: 'Usuário',
            attribute: 'user_name',
            isActive: false
        }, {
            matColumnDef: 'active',
            title: 'Ativo',
            attribute: 'active',
            isActive: true
        }];
    }

    populateList() {
        if (this.idCompany != null) {
            this.service.getByCompany(this.idCompany).subscribe((result: any[]) => {
                this.listOriginal = result.sort((a: any, b: any) => {
                    return a.company_name.localeCompare(b.company_name) //
                        || a.role_name.localeCompare(b.role_name) //
                        || a.user_name.localeCompare(b.user_name);
                });
            });
        } else if (this.idOwner != null) {
            this.service.getByOwner(this.idOwner).subscribe((result: any[]) => {
                this.listOriginal = result.sort((a: any, b: any) => {
                    return a.owner_name.localeCompare(b.owner_name) //
                        || a.role_name.localeCompare(b.role_name) //
                        || a.user_name.localeCompare(b.user_name);
                });
            });
        }
    }

    deleteItem(item) {
        this.service.delete(item.id).subscribe(() => {
            this.generic.toastr.success('Exclusão executada com sucesso', this.pageTitle());
            this.populateList();
        });
    }

    getRouteBack() {
        if (this.idCompany != null)
            return 'company';
        else if (this.idOwner != null)
            return 'user';
    }

}