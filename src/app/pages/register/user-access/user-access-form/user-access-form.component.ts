import { ThrowStmt } from '@angular/compiler';
import { Component, Injector } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { CompanyService } from 'src/app/services/company/company.service';
import { RoleService } from 'src/app/services/role/role.service';
import { OwnerService } from '../../../../services/company/owner.service';
import { GenericFormPage } from '../../../generic/generic-form.page';
import { UserService } from '../../../../services/user/user.service';
import { UserAccessService } from '../../../../services/user-access/user-access.service';

@Component({
    templateUrl: './user-access-form.component.html',
    styleUrls: ['./user-access-form.component.scss']
})
export class UserAccessFormComponent extends GenericFormPage {

    // public idOwner;

    public responsible: FormControl = new FormControl('');
    public roleEdit: FormControl = new FormControl('');
    public userEdit: FormControl = new FormControl('');
    public activeEdit: FormControl = new FormControl('');

    public cpfSearch: FormControl = new FormControl('');
    public cpfFound: boolean;
    public cpfMessage: string = '';

    public showFormNewUser: boolean = false;
    public showFormExistUser: boolean = false;

    public hide1: boolean = true;
    public hide2: boolean = true;

    public roleList: any[];

    constructor(
        private roleService: RoleService,
        private ownerService: OwnerService,
        private companyService: CompanyService,
        private userAccessService: UserAccessService,
        private userService: UserService,
        public injector: Injector
    ) {
        super(injector);
        this.imageFieldName = 'newUser_image';
    }

    onInit(): void {
        if (this.idCompany != null) {
            this.companyService.getById(this.idCompany).subscribe(resultCompany => {
                let model = !!resultCompany ? resultCompany[0] : {};
                this.responsible = new FormControl(model?.name);

                this.roleService.get().subscribe((resultRole: any) => {
                    this.roleList = resultRole;
                    if (model.level == 1)
                        this.roleList = this.roleList.filter(item => !!item.is_admin);
                });
            });
        } else if (this.idOwner != null) {
            this.ownerService.getById(this.idOwner).subscribe(resultOwner => {
                let model: any = !!resultOwner ? resultOwner : {};
                this.responsible = new FormControl(model?.name);

                this.roleService.get().subscribe((resultRole: any) => {
                    this.roleList = resultRole.filter(item => !!item.is_admin);
                });
            });
        }
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public pageTitle(): string {
        return 'Acesso de usuário';
    }

    public pageMain(): string {
        if (this.idCompany != null)
            return `userAccess/byCompany/${this.idCompany}`;
        else if (this.idOwner != null)
            return `userAccess/byOwner/${this.idOwner}`;
    }

    public setForm(): void {
        this.form = new FormGroup({
            existUser_id: new FormControl(''),
            existUser_cpf: new FormControl(''),
            existUser_name: new FormControl(''),
            existUser_username: new FormControl(''),
            existUser_email: new FormControl(''),
            existUser_active: new FormControl(''),
            existUser_activeValid: new FormControl(null, [Validators.required]),
            existUser_role: new FormControl(null, [Validators.required]),
            //
            newUser_username: new FormControl('', [Validators.required]),
            newUser_email: new FormControl('', [Validators.required, Validators.email]),
            newUser_cpf: new FormControl('', [Validators.required]),
            newUser_name: new FormControl('', [Validators.required]),
            newUser_phone_ddd: new FormControl('', [Validators.required]),
            newUser_phone: new FormControl('', [Validators.required]),
            newUser_address: new FormControl('', [Validators.required]),
            newUser_district: new FormControl('', [Validators.required]),
            newUser_zip_code: new FormControl('', [Validators.required]),
            newUser_city: new FormControl('', [Validators.required]),
            newUser_state: new FormControl('', [Validators.required]),
            newUser_country: new FormControl('', [Validators.required]),
            newUser_image: new FormControl(''),
            newUser_active: new FormControl('', [Validators.required]),
            newUser_role: new FormControl(null, [Validators.required]),
            //
            editUser_role: new FormControl(''),
            editUser_user: new FormControl(''),
            editUser_active: new FormControl(''),
        });
    }

    public getById(): void {
        this.userAccessService.getById(this.id).subscribe(result => {
            this.model = !!result ? result[0] : {};

            for (let control of Object.keys(this.form.controls)) {
                if (control.indexOf('newUser') >= 0 || control.indexOf('existUser') >= 0) {
                    this.form.removeControl(control);
                }
            }

            this.form.patchValue({
                editUser_role: this.model.role_name,
                editUser_user: this.model.user_name,
                editUser_active: this.model.active
            });
        });
    }

    public save(): void {
        if (!!this.isEdit) {
            let bodyUserAccess = {
                active: this.form.get('editUser_active').value
            }

            this.userAccessService.put(this.id, bodyUserAccess).subscribe(result => {
                this.generic.toastr.success(`Usuário x Acesso editado com sucesso`, this.pageTitle());
                this.generic.router.navigateByUrl(`/${this.pageMain()}`);
            });
        } else {
            if (!!this.cpfFound) {
                let idUser = this.form.get('existUser_id').value;

                let bodyUser = {
                    active: this.form.get('existUser_active').value
                }

                this.userService.put(idUser, bodyUser).subscribe(userResult => {
                    let bodyUserAccess = {
                        id_user: idUser,
                        id_role: this.form.get('existUser_role').value,
                        id_owner: (this.idCompany != null ? null : this.idOwner),
                        id_company: this.idCompany ?? null,
                        active: true
                    }

                    this.userAccessService.post(bodyUserAccess).subscribe(userAccessResult => {
                        this.generic.toastr.success(`Usuário editado e associado com sucesso`, this.pageTitle());
                        this.generic.router.navigateByUrl(`/${this.pageMain()}`);
                    });
                });
            } else {
                let bodyUser = {
                    username: this.form.get('newUser_username').value,
                    email: this.form.get('newUser_email').value,
                    cpf: this.form.get('newUser_cpf').value,
                    name: this.form.get('newUser_name').value,
                    phone_ddd: this.form.get('newUser_phone_ddd').value,
                    phone: this.form.get('newUser_phone').value,
                    address: this.form.get('newUser_address').value,
                    district: this.form.get('newUser_district').value,
                    zip_code: this.form.get('newUser_zip_code').value,
                    city: this.form.get('newUser_city').value,
                    state: this.form.get('newUser_state').value,
                    country: this.form.get('newUser_country').value,
                    password: this.form.get('newUser_cpf').value,
                    image: this.form.get('newUser_image').value,
                    active: this.form.get('newUser_active').value
                }

                this.userService.post(bodyUser).subscribe((userResult: any) => {
                    let bodyUserAccess = {
                        id_user: userResult.id,
                        id_role: this.form.get('newUser_role').value,
                        id_owner: (this.idCompany != null ? null : this.idOwner),
                        id_company: this.idCompany ?? null,
                        active: true
                    }

                    this.userAccessService.post(bodyUserAccess).subscribe(userAccessResult => {
                        this.generic.toastr.success(`Usuário criado e associado com sucesso`, this.pageTitle());
                        this.generic.router.navigateByUrl(`/${this.pageMain()}`);
                    });
                });
            }
        }
    }

    public patchImage(image: any): void {
        this.form.patchValue({
            'newUser_image': image
        });
    }

    searchCpf() {
        this.cpfFound = this.showFormExistUser = this.showFormNewUser = false;
        this.cpfMessage = '';

        if (this.cpfSearch.valid && !!this.cpfSearch.value) {
            this.userService.getByCpf(this.cpfSearch.value).subscribe((userObj: any) => {
                let message, title;
                if (!!userObj && !!userObj.id) {
                    title = 'Usuário encontrado!';

                    if (!!userObj.active)
                        message = 'Para associar o mesmo à empresa responsável basta salvar o formulário abaixo';
                    else
                        message = 'Para associar o mesmo à empresa responsável, primeiro ative-o e depois basta salvar o formulário abaixo';

                    this.generic.toastr.info(message, title);
                    this.cpfMessage = `${title} ${message}`;
                    this.cpfFound = true;
                    this.createUser(userObj);
                } else {
                    title = 'Usuário NÃO encontrado!';
                    message = 'Caso queira criar um novo usuário para o CPF informado, clique no botão "Criar novo usuário"';
                    this.generic.toastr.warning(message, title);
                    this.cpfMessage = `${title} ${message}`;
                    this.cpfFound = this.showFormExistUser = this.showFormNewUser = false;
                }
            });
        } else {
            this.generic.toastr.error('Informe um CPF em um formato válido para realizar a pesquisa', 'Formato inválido');
            this.cpfFound = this.showFormExistUser = this.showFormNewUser = false;
            this.cpfMessage = '';
        }
    }

    createUser(userObj?: any) {
        this.showFormExistUser = this.showFormNewUser = false;
        this.setForm();

        if (!!userObj) {
            for (let control of Object.keys(this.form.controls)) {
                if (control.indexOf('newUser') >= 0) {
                    this.form.removeControl(control);
                }
            }

            this.form.patchValue({
                existUser_id: userObj.id,
                existUser_cpf: userObj.cpf,
                existUser_name: userObj.name,
                existUser_username: userObj.username,
                existUser_email: userObj.email,
                existUser_active: userObj.active,
                existUser_activeValid: !!userObj.active ? true : null
            });

            this.showFormExistUser = true;
        } else {
            for (let control of Object.keys(this.form.controls)) {
                if (control.indexOf('existUser') >= 0) {
                    this.form.removeControl(control);
                }
            }

            this.form.patchValue({
                newUser_cpf: this.cpfSearch.value,
                newUser_active: true,
                existUser_activeValid: true
            });

            this.showFormNewUser = true;
        }
    }

    changeActive(event) {
        if (!!this.cpfFound) {
            this.form.patchValue({
                existUser_activeValid: !!event ? true : null
            });
        } else {
            this.form.patchValue({
                newUser_activeValid: !!event ? true : null
            });
        }
    }

}