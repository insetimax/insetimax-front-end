import { Component, Injector } from '@angular/core';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { GenericPage } from '../../generic/generic.page';
import { PlaceActCategoryService } from '../../../services/place-act-category/place-act-category.service';
import { PlaceActCategoryProductService } from '../../../services/place-act-category-product/place-act-category-product.service';

@Component({
    templateUrl: './place-act-category-product.component.html',
    styleUrls: ['./place-act-category-product.component.scss']
})
export class PlaceActCategoryProductComponent extends GenericPage {

    public idPlace: number;
    public idPlaceActCategory: number;
    public placeActCategory: any;

    constructor(
        private placeActCategoryService: PlaceActCategoryService,
        private service: PlaceActCategoryProductService,
        public injector: Injector
    ) {
        super(injector);
    }

    public onInit(): void {
        this.idPlaceActCategory = this.activatedRoute.snapshot.params?.idPlaceActCategory;
        if (!!this.idPlaceActCategory) {
            this.placeActCategoryService.getById(this.idPlaceActCategory).subscribe(result => {
                this.placeActCategory = !!result ? result[0] : {};
                this.idPlace = this.placeActCategory.id_place;
                this.idCompany = this.placeActCategory.place.id_company;
            });
        }
    }

    public pageTitle(): string {
        return 'Lugar/Comodo x Atividade x Produtos';
    }

    public pageForm(): string {
        return `placeActCategoryProductForm/${this.idPlaceActCategory}`;
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public setColumns() {
        this.columns = [{
            matColumnDef: 'id',
            title: 'ID',
            attribute: 'id',
            isActive: false
        }, {
            matColumnDef: 'company',
            title: 'Empresa responsável',
            attribute: 'placeActCategory.place.company.name',
            isActive: false
        }, {
            matColumnDef: 'place',
            title: 'Lugar/Comodo',
            attribute: 'placeActCategory.place.name',
            isActive: false
        }, {
            matColumnDef: 'act_category',
            title: 'Atividade',
            attribute: 'placeActCategory.actCategory.name',
            isActive: false
        }, {
            matColumnDef: 'product',
            title: 'Produto',
            attribute: 'product.name',
            isActive: false
        }, {
            matColumnDef: 'active',
            title: 'Ativo',
            attribute: 'active',
            isActive: true
        }];
    }

    populateList() {
        this.service.getByPlaceActCategory(this.idPlaceActCategory).subscribe((result: any[]) => {
            this.listOriginal = result.sort((a: any, b: any) => {
                return a.placeActCategory.place.name.localeCompare(b.place.name) //
                    || a.placeActCategory.actCategory.name.localeCompare(b.actCategory.name) //
                    || a.product.name.localeCompare(b.product.name);
            });
        });
    }

    deleteItem(item) {
        this.service.delete(item.id).subscribe(() => {
            this.generic.toastr.success('Exclusão executada com sucesso', this.pageTitle());
            this.populateList();
        });
    }

}
