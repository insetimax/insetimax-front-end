import { Component, Injector, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { GenericFormPage } from '../../../generic/generic-form.page';
import { PlaceActCategoryService } from '../../../../services/place-act-category/place-act-category.service';
import { ProductService } from '../../../../services/product/product.service';
import { PlaceActCategoryProductService } from '../../../../services/place-act-category-product/place-act-category-product.service';

@Component({
    templateUrl: './place-act-category-product-form.component.html',
    styleUrls: ['./place-act-category-product-form.component.scss']
})
export class PlaceActCategoryProductFormComponent extends GenericFormPage {

    public idPlaceActCategory: number;
    public placeActCategory;

    public companyForm: FormControl = new FormControl('');
    public placeForm: FormControl = new FormControl('');
    public actCategoryForm: FormControl = new FormControl('');
    public productForm: FormControl = new FormControl('');

    public productList: any[] = [];

    constructor(
        private productService: ProductService,
        private placeActCategoryService: PlaceActCategoryService,
        private service: PlaceActCategoryProductService,
        public injector: Injector
    ) {
        super(injector);
    }

    onInit(): void {
        this.idPlaceActCategory = this.activatedRoute.snapshot.params?.idPlaceActCategory;
        if (!!this.idPlaceActCategory) {
            this.placeActCategoryService.getById(this.idPlaceActCategory).subscribe(result => {
                this.placeActCategory = !!result ? result[0] : {};
                this.companyForm = new FormControl(this.placeActCategory.place.company.name);
                this.placeForm = new FormControl(this.placeActCategory.place.name);
                this.actCategoryForm = new FormControl(this.placeActCategory.actCategory.name);

                this.productService.getByOwner(this.placeActCategory.place.company.id_owner).subscribe((productResult: any) => {
                    this.productList = productResult;
                });
            });
        }
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public pageTitle(): string {
        return 'Lugar/Comodo x Atividade x Produto';
    }

    public pageMain(): string {
        return `placeActCategoryProduct/${this.idPlaceActCategory}`;
    }

    public setForm(): void {
        this.form = new FormGroup({
            id_product: new FormControl('', [Validators.required]),
            info_use: new FormControl('', [Validators.required]),
            info: new FormControl('', [Validators.required]),
            active: new FormControl(true, [Validators.required])
        });
    }

    public getById(): void {
        this.service.getById(this.id).subscribe(result => {
            this.model = !!result ? result[0] : {};
            this.productForm = new FormControl(this.model?.product?.name);

            this.form.setValue({
                id_product: this.model.id_product,
                info_use: this.model.info_use,
                info: this.model.info,
                active: this.model.active
            });
        });
    }

    public save(): void {
        let body = {
            id_place_act_category: this.idPlaceActCategory,
            id_product: this.form.get('id_product').value,
            info_use: this.form.get('info_use').value,
            info: this.form.get('info').value,
            active: this.form.get('active').value
        }

        if (!!this.isEdit) {
            this.service.put(this.id, body).subscribe(result => {
                this.generic.toastr.success(`Registro editado com sucesso`, this.pageTitle());
                this.generic.router.navigateByUrl(`/${this.pageMain()}`);
            });
        } else {
            this.service.post(body).subscribe(result => {
                this.generic.toastr.success(`Registro criada com sucesso`, this.pageTitle());
                this.generic.router.navigateByUrl(`/${this.pageMain()}`);
            });
        }
    }

    public patchImage(image: any): void {
    }

}