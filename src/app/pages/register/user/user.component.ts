import { Component, Injector } from '@angular/core';
import { PageHeaderOptionButton } from 'src/app/components/page/page-header/page-header.component';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { GenericPage } from '../../generic/generic.page';
import { UserService } from '../../../services/user/user.service';

@Component({
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent extends GenericPage {

    public idUser: number;
    public phOptionButton: PageHeaderOptionButton[];

    constructor(
        private service: UserService,
        public injector: Injector
    ) {
        super(injector);
    }

    public onInit(): void {
        this.idUser = this.generic.getUser().id;
    }

    public pageTitle(): string {
        return 'Usuários';
    }

    public pageForm(): string {
        return `userForm`;
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public setColumns() {
        this.columns = [{
            matColumnDef: 'id',
            title: 'ID',
            attribute: 'id',
        }, {
            matColumnDef: 'cpf',
            title: 'CPF',
            attribute: 'cpf',
            mask: '000.000.000-00'
        }, {
            matColumnDef: 'name',
            title: 'Nome',
            attribute: 'name',
        }, {
            matColumnDef: 'accesses_name',
            title: 'Acessos',
            attribute: 'accesses_name',
            isList: true
        }, {
            matColumnDef: 'active',
            title: 'Ativo',
            attribute: 'active',
            isActive: true
        }];

        if (this.idCompany == null && this.idOwner != null) {
            this.phOptionButton = [{
                id: 'userAccessByOwner',
                title: `Acessos de usuários da ${this.userAccess.owner.name}`,
                icon: 'person',
                route: `/userAccess/byOwner/${this.idOwner}`
            }];
        }
    }

    populateList() {
        let userAccess = this.generic.getUserAccess();
        if (userAccess.id_company != null) {
            this.service.getAllUsersOfCompany(this.idUser).subscribe((result: any[]) => {
                this.listOriginal = result;
            });
        } else {
            this.service.getAllUsersOfOwner(this.idUser).subscribe((result: any[]) => {
                this.listOriginal = result;
            });
        }
    }

    deleteItem(item) {
        if (item.id == this.idUser) {
            this.generic.toastr.warning('NÃO é possível excluir seu próprio usuário', this.pageTitle());
        } else {
            this.service.delete(item.id).subscribe(() => {
                this.generic.toastr.success('Exclusão executada com sucesso', this.pageTitle());
                this.populateList();
            });
        }
    }

}