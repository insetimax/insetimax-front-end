import { Component, Injector, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { GenericFormPage } from '../../../generic/generic-form.page';
import { UserService } from '../../../../services/user/user.service';

@Component({
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent extends GenericFormPage {

    public idUser: number;

    constructor(
        private service: UserService,
        public injector: Injector
    ) {
        super(injector);
    }

    onInit(): void {
        this.idUser = this.generic.getUser().id;
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public pageTitle(): string {
        return 'Usuários';
    }

    public pageMain(): string {
        return 'user';
    }

    public setForm(): void {
        this.form = new FormGroup({
            username: new FormControl('', [Validators.required]),
            email: new FormControl('', [Validators.required, Validators.email]),
            cpf: new FormControl('', [Validators.required]),
            name: new FormControl('', [Validators.required]),
            phone_ddd: new FormControl('', [Validators.required]),
            phone: new FormControl('', [Validators.required]),
            address: new FormControl('', [Validators.required]),
            district: new FormControl('', [Validators.required]),
            zip_code: new FormControl('', [Validators.required]),
            city: new FormControl('', [Validators.required]),
            state: new FormControl('', [Validators.required]),
            country: new FormControl('', [Validators.required]),
            image: new FormControl(''),
            active: new FormControl(true, [Validators.required])
        });
    }

    public getById(): void {
        this.service.getById(this.id).subscribe(result => {
            this.model = !!result ? result : {};
            this.title += ` > ${this.model?.name}`;

            this.form.setValue({
                username: this.model.username,
                email: this.model.email,
                cpf: this.model.cpf,
                name: this.model.name,
                phone_ddd: this.model.phone_ddd,
                phone: this.model.phone,
                address: this.model.address,
                district: this.model.district,
                zip_code: this.model.zip_code,
                city: this.model.city,
                state: this.model.state,
                country: this.model.country,
                image: this.model.image,
                active: this.model.active
            });
        });
    }

    public save(): void {
        let body = {
            username: this.form.get('username').value,
            email: this.form.get('email').value,
            cpf: this.form.get('cpf').value,
            name: this.form.get('name').value,
            phone_ddd: this.form.get('phone_ddd').value,
            phone: this.form.get('phone').value,
            address: this.form.get('address').value,
            district: this.form.get('district').value,
            zip_code: this.form.get('zip_code').value,
            city: this.form.get('city').value,
            state: this.form.get('state').value,
            country: this.form.get('country').value,
            image: this.form.get('image').value,
            active: this.form.get('active').value
        }

        if (!!this.isEdit) {
            if (this.idUser == this.id && !this.form.get('active')) {
                this.generic.toastr.warning('NÃO é possível inativar seu próprio usuário', this.pageTitle());
            } else {
                this.service.put(this.id, body).subscribe(result => {
                    this.generic.toastr.success(`Registro editado com sucesso`, this.pageTitle());
                    this.generic.router.navigateByUrl(`/${this.pageMain()}`);
                });
            }
        }
    }

    public patchImage(image: any): void {
        this.form.patchValue({
            'image': image
        });
    }

}
