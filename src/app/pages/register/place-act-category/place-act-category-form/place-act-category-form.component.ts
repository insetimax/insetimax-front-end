import { Component, Injector } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { ActivityCategoryService } from '../../../../services/activity-category/activity-category.service';
import { GenericFormPage } from '../../../generic/generic-form.page';
import { PlaceService } from '../../../../services/place/place.service';
import { PlaceActCategoryService } from '../../../../services/place-act-category/place-act-category.service';

@Component({
    templateUrl: './place-act-category-form.component.html',
    styleUrls: ['./place-act-category-form.component.scss']
})
export class PlaceActCategoryFormComponent extends GenericFormPage {

    public idPlace: number;
    public place;

    public companyForm: FormControl = new FormControl('');
    public placeForm: FormControl = new FormControl('');
    public actCategoryForm: FormControl = new FormControl('');

    public actCategoryList: any[] = [];

    constructor(
        private actCategoryService: ActivityCategoryService,
        private placeService: PlaceService,
        private service: PlaceActCategoryService,
        public injector: Injector
    ) {
        super(injector);
    }

    onInit(): void {
        this.idPlace = this.activatedRoute.snapshot.params?.idPlace;
        if (!!this.idPlace) {
            this.placeService.getById(this.idPlace).subscribe(result => {
                this.place = !!result ? result[0] : {};
                this.companyForm = new FormControl(this.place.company.name);
                this.placeForm = new FormControl(this.place.name);

                this.actCategoryService.getByOwner(this.place.company.id_owner).subscribe((actCategResult: any) => {
                    this.actCategoryList = actCategResult;
                });
            });
        }
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public pageTitle(): string {
        return 'Lugar/Comodo x Atividade';
    }

    public pageMain(): string {
        return `placeActCategory/${this.idPlace}`;
    }

    public setForm(): void {
        this.form = new FormGroup({
            id_act_category: new FormControl('', [Validators.required]),
            active: new FormControl(true, [Validators.required])
        });
    }

    public getById(): void {
        this.service.getById(this.id).subscribe(result => {
            this.model = !!result ? result[0] : {};
            this.actCategoryForm = new FormControl(this.model?.actCategory?.name);

            this.form.setValue({
                id_act_category: this.model.id_act_category,
                active: this.model.active
            });
        });
    }

    public save(): void {
        let body = {
            id_place: this.idPlace,
            id_act_category: this.form.get('id_act_category').value,
            active: this.form.get('active').value
        }

        if (!!this.isEdit) {
            this.service.put(this.id, body).subscribe(result => {
                this.generic.toastr.success(`Registro editado com sucesso`, this.pageTitle());
                this.generic.router.navigateByUrl(`/${this.pageMain()}`);
            });
        } else {
            this.service.post(body).subscribe(result => {
                this.generic.toastr.success(`Registro criada com sucesso`, this.pageTitle());
                this.generic.router.navigateByUrl(`/${this.pageMain()}`);
            });
        }
    }

    public patchImage(image: any): void {
    }

}