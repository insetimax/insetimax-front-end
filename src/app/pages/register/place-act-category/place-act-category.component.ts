import { Component, Injector } from '@angular/core';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { GenericPage } from '../../generic/generic.page';
import { PlaceService } from '../../../services/place/place.service';
import { PlaceActCategoryService } from '../../../services/place-act-category/place-act-category.service';

@Component({
    templateUrl: './place-act-category.component.html',
    styleUrls: ['./place-act-category.component.scss']
})
export class PlaceActCategoryComponent extends GenericPage {

    public idPlace: number;
    public place: any;

    constructor(
        private placeService: PlaceService,
        private service: PlaceActCategoryService,
        public injector: Injector
    ) {
        super(injector);
    }

    public onInit(): void {
        this.idPlace = this.activatedRoute.snapshot.params?.idPlace;
        if (!!this.idPlace) {
            this.placeService.getById(this.idPlace).subscribe(result => {
                this.place = !!result ? result[0] : {};
                this.idCompany = this.place.id_company;
            });
        }
    }

    public pageTitle(): string {
        return 'Lugar/Comodo x Atividades';
    }

    public pageForm(): string {
        return `placeActCategoryForm/${this.idPlace}`;
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public setColumns() {
        this.columns = [{
            matColumnDef: 'id',
            title: 'ID',
            attribute: 'id',
            isActive: false
        }, {
            matColumnDef: 'company',
            title: 'Empresa responsável',
            attribute: 'place.company.name',
            isActive: false
        }, {
            matColumnDef: 'place',
            title: 'Lugar/Comodo',
            attribute: 'place.name',
            isActive: false
        }, {
            matColumnDef: 'act_category',
            title: 'Atividade',
            attribute: 'actCategory.name',
            isActive: false
        }, {
            matColumnDef: 'active',
            title: 'Ativo',
            attribute: 'active',
            isActive: true
        }];

        this.optionColumns = [{
            id: 'placeActCategoryProduct',
            title: 'Produtos',
            route: 'placeActCategoryProduct'
        }];
    }

    populateList() {
        this.service.getByPlace(this.idPlace).subscribe((result: any[]) => {
            this.listOriginal = result.sort((a: any, b: any) => {
                return a.place.name.localeCompare(b.place.name) //
                    || a.actCategory.name.localeCompare(b.actCategory.name);
            });
        });
    }

    deleteItem(item) {
        this.service.delete(item.id).subscribe(() => {
            this.generic.toastr.success('Exclusão executada com sucesso', this.pageTitle());
            this.populateList();
        });
    }

}
