import { Component, OnInit } from '@angular/core';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { GenericService } from 'src/app/services/generic/generic.service';

@Component({
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    public pageList = [{
        id: 'activityCategory',
        name: 'Atividades',
        route: 'activityCategory',
        role: 'owner'
    }, {
        id: 'products',
        name: 'Produtos',
        route: 'product',
        role: 'owner'
    }, {
        id: 'companies',
        name: 'Empresas',
        route: 'company',
        role: 'all'
    }, {
        id: 'users',
        name: 'Usuários',
        route: 'user',
        role: 'all'
    }];

    constructor(
        private generic: GenericService
    ) {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    ngOnInit(): void {
        let userAccess = this.generic.getUserAccess();
        if (!!userAccess?.role?.is_admin) {
            if (userAccess.id_company != null)
                this.pageList = this.pageList.filter(item => item.role == 'all')
        } else {
            this.pageList = [];
        }
    }

    goToPage(page) {
        this.generic.goToPage(page);
    }

}