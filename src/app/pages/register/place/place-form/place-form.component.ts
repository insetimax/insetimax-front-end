import { Component, Injector } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { CompanyService } from 'src/app/services/company/company.service';
import { GenericFormPage } from '../../../generic/generic-form.page';
import { PlaceService } from '../../../../services/place/place.service';

@Component({
    templateUrl: './place-form.component.html',
    styleUrls: ['./place-form.component.scss']
})
export class PlaceFormComponent extends GenericFormPage {

    public company: FormControl = new FormControl('');

    constructor(
        private companyService: CompanyService,
        private service: PlaceService,
        public injector: Injector
    ) {
        super(injector);
    }

    onInit(): void {
        if (this.idCompany != null) {
            this.companyService.getById(this.idCompany).subscribe(result => {
                let model = !!result ? result[0] : {};
                this.company = new FormControl(model?.name);
            });
        }
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public pageTitle(): string {
        return 'Lugar/Comodo';
    }

    public pageMain(): string {
        return `place/${this.idCompany}`;
    }

    public setForm(): void {
        this.form = new FormGroup({
            name: new FormControl('', [Validators.required]),
            size_m2: new FormControl('', [Validators.required]),
            active: new FormControl(true, [Validators.required])
        });
    }

    public getById(): void {
        this.service.getById(this.id).subscribe(result => {
            this.model = !!result ? result[0] : {};
            this.title += ` > ${this.model?.name}`;

            this.form.setValue({
                name: this.model.name,
                size_m2: this.model.size_m2,
                active: this.model.active
            });
        });
    }

    public save(): void {
        let body = {
            id_company: this.idCompany,
            name: this.form.get('name').value,
            size_m2: this.form.get('size_m2').value,
            active: this.form.get('active').value
        }

        if (!!this.isEdit) {
            this.service.put(this.id, body).subscribe(result => {
                this.generic.toastr.success(`Registro editado com sucesso`, this.pageTitle());
                this.generic.router.navigateByUrl(`/${this.pageMain()}`);
            });
        } else {
            this.service.post(body).subscribe(result => {
                this.generic.toastr.success(`Registro criada com sucesso`, this.pageTitle());
                this.generic.router.navigateByUrl(`/${this.pageMain()}`);
            });
        }
    }

    public patchImage(image: any): void {
    }

}
