import { Component, ComponentFactoryResolver, Injector } from '@angular/core';
import jsPDF from 'jspdf';
import { PageHeaderOptionButton } from 'src/app/components/page/page-header/page-header.component';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { PlaceService } from '../../../services/place/place.service';
import { GenericPage } from '../../generic/generic.page';

@Component({
    templateUrl: './place.component.html',
    styleUrls: ['./place.component.scss']
})
export class PlaceComponent extends GenericPage {

    public qrCodeValue: string = '';

    public optionButton: PageHeaderOptionButton[] = [{
        id: 'qrcode',
        title: 'Imprimir todos os QR Codes',
        icon: 'assignment',
        event: true
    }];

    constructor(
        private readonly resolver: ComponentFactoryResolver,
        private service: PlaceService,
        public injector: Injector
    ) {
        super(injector);
    }

    public onInit(): void {
    }

    public pageTitle(): string {
        return 'Lugares/Comodos';
    }

    public pageForm(): string {
        return `placeForm/${this.idCompany}`;
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public setColumns() {
        this.columns = [{
            matColumnDef: 'id',
            title: 'ID',
            attribute: 'id',
            isActive: false
        }, {
            matColumnDef: 'company',
            title: 'Empresa responsável',
            attribute: 'company.name',
            isActive: false
        }, {
            matColumnDef: 'name',
            title: 'Nome',
            attribute: 'name',
            isActive: false
        }, {
            matColumnDef: 'size_m2',
            title: 'Tamanho (m²)',
            attribute: 'size_m2',
            isActive: false
        }, {
            matColumnDef: 'active',
            title: 'Ativo',
            attribute: 'active',
            isActive: true
        }];

        this.optionColumns = [{
            id: 'placeActCategory',
            title: 'Atividades',
            route: 'placeActCategory'
        }, {
            id: 'qrcode',
            title: 'Imprimir QR-CODE',
            event: true
        }];
    }

    populateList() {
        this.service.getByCompany(this.idCompany).subscribe((result: any[]) => {
            this.listOriginal = result.sort((a: any, b: any) => a.name.localeCompare(b.name));
        });
    }

    deleteItem(item) {
        this.service.delete(item.id).subscribe(() => {
            this.generic.toastr.success('Exclusão executada com sucesso', this.pageTitle());
            this.populateList();
        });
    }

    clickOption(ret) {
        if (ret.opt.id == 'qrcode') {
            this.downloadQrCode(ret.item);
        }
    }

    clickOptionHeader(opt) {
        if (opt.id == 'qrcode') {
            const list = this.listOriginal.filter((item: any) => !!item.active);
            this.generatePDF(list, 'ativos');
        }
    }

    downloadQrCode(item) {
        const list = [item];
        this.generatePDF(list, item.name);
    }

    private generatePDF(list, name) {
        let doc: any = new jsPDF();

        const pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
        const controlmax: any = document.getElementById('layout');

        let qrcode;
        let cont = 0;
        for (let item of list) {
            if (cont >= 1) {
                doc.addPage();
            }

            qrcode = document.getElementById(`qrcode-${item.id}`);

            doc.addImage(this.getBase64Image(controlmax), 'JPG', 10, 10);
            doc.addImage(this.getBase64Image(qrcode.firstChild.firstChild), 'JPG', 50, 50, 93, 93);

            doc.setFont('Arial');
            doc.setFontSize(25);
            doc.text(item.name, (pageWidth / 2) - 8, 158, { align: 'center' });

            cont++;
        }

        doc.save(`QR-CODE_${name}_${(new Date()).toISOString()}.pdf`);
    }

    private getBase64Image(img: any) {
        var canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL('image/png');
        return dataURL;
    }

}