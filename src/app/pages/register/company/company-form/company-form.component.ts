import { Component, Injector } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { CompanyService } from 'src/app/services/company/company.service';
import { GenericFormPage } from '../../../generic/generic-form.page';

@Component({
    templateUrl: './company-form.component.html',
    styleUrls: ['./company-form.component.scss']
})
export class CompanyFormComponent extends GenericFormPage {

    public isOwner: boolean = true;
    public responsible: FormControl = new FormControl('');

    constructor(
        private service: CompanyService,
        public injector: Injector
    ) {
        super(injector);
    }

    onInit(): void {
        if (this.userAccess.id_company != null) {
            this.responsible = new FormControl(this.userAccess.company.name);
            this.isOwner = false;
        } else if (this.userAccess.id_owner != null) {
            this.responsible = new FormControl(this.userAccess.owner.name);
            this.isOwner = true;
        }
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public pageTitle(): string {
        return 'Empresa';
    }

    public pageMain(): string {
        return 'company';
    }

    public setForm(): void {
        this.form = new FormGroup({
            cnpj: new FormControl('', [Validators.required]),
            name: new FormControl('', [Validators.required]),
            phone_ddd: new FormControl('', [Validators.required]),
            phone: new FormControl('', [Validators.required]),
            address: new FormControl('', [Validators.required]),
            district: new FormControl('', [Validators.required]),
            zip_code: new FormControl('', [Validators.required]),
            city: new FormControl('', [Validators.required]),
            state: new FormControl('', [Validators.required]),
            country: new FormControl('', [Validators.required]),
            qtt_users: new FormControl(1, [Validators.required, Validators.min(0)]),
            qtt_users_child: new FormControl(1, [Validators.required, Validators.min(0)]),
            qtt_child: new FormControl(1, [Validators.required, Validators.min(0)]),
            image: new FormControl(''),
            active: new FormControl(true, [Validators.required])
        });
    }

    public getById(): void {
        this.service.getById(this.id).subscribe(result => {
            this.model = !!result ? result[0] : {};
            this.title += ` > ${this.model?.name}`;

            this.form.setValue({
                cnpj: this.model.cnpj,
                name: this.model.name,
                phone_ddd: this.model.phone_ddd,
                phone: this.model.phone,
                address: this.model.address,
                district: this.model.district,
                zip_code: this.model.zip_code,
                city: this.model.city,
                state: this.model.state,
                country: this.model.country,
                qtt_users: this.model.qtt_users,
                qtt_users_child: this.model.qtt_users_child,
                qtt_child: this.model.qtt_child,
                image: this.model.image,
                active: this.model.active
            });
        });
    }

    public save(): void {
        let body = {
            id_owner: this.userAccess.id_owner,
            id_company: (!this.isOwner ? this.userAccess.id_company : null),
            cnpj: this.form.get('cnpj').value,
            name: this.form.get('name').value,
            phone_ddd: this.form.get('phone_ddd').value,
            phone: this.form.get('phone').value,
            address: this.form.get('address').value,
            district: this.form.get('district').value,
            zip_code: this.form.get('zip_code').value,
            city: this.form.get('city').value,
            state: this.form.get('state').value,
            country: this.form.get('country').value,
            qtt_users: this.form.get('qtt_users').value,
            qtt_child: this.form.get('qtt_child').value ?? 0,
            qtt_users_child: this.form.get('qtt_users_child').value ?? 0,
            image: this.form.get('image').value,
            active: this.form.get('active').value
        }

        if (!!this.isEdit) {
            this.service.put(this.id, body).subscribe(result => {
                this.generic.toastr.success(`Registro editado com sucesso`, this.pageTitle());
                this.generic.router.navigateByUrl(`/${this.pageMain()}`);
            });
        } else {
            this.service.post(body).subscribe(result => {
                this.generic.toastr.success(`Registro criada com sucesso`, this.pageTitle());
                this.generic.router.navigateByUrl(`/${this.pageMain()}`);
            });
        }
    }

    public patchImage(image: any): void {
        this.form.patchValue({
            'image': image
        });
    }

}
