import { Component, Injector } from '@angular/core';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { CompanyService } from 'src/app/services/company/company.service';
import { GenericPage } from '../../generic/generic.page';

@Component({
    templateUrl: './company.component.html',
    styleUrls: ['./company.component.scss']
})
export class CompanyComponent extends GenericPage {

    constructor(
        private service: CompanyService,
        public injector: Injector
    ) {
        super(injector);
    }

    public onInit(): void {
    }

    public pageTitle(): string {
        return 'Empresas';
    }

    public pageForm(): string {
        return 'companyForm';
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public setColumns() {
        this.columns = [{
            matColumnDef: 'id',
            title: 'ID',
            attribute: 'id',
            isActive: false
        }, {
            matColumnDef: 'company',
            title: 'Empresa responsável',
            attribute: 'company.name',
            attributeElse: 'owner.name',
            isActive: false
        }, {
            matColumnDef: 'cnpj',
            title: 'CNPJ',
            attribute: 'cnpj',
            isActive: false,
            mask: '000.000.000/0000-00'
        }, {
            matColumnDef: 'name',
            title: 'Nome',
            attribute: 'name',
            isActive: false
        }];

        if (this.userAccess.id_company == null && this.userAccess.id_owner) {
            this.columns.push({
                matColumnDef: 'qtt_users',
                title: 'Qtd. usuários para empresa',
                attribute: 'qtt_users',
                isActive: false
            }, {
                matColumnDef: 'qtt_child',
                title: 'Qtd. empresas filhas',
                attribute: 'qtt_child',
                isActive: false
            }, {
                matColumnDef: 'qtt_users_child',
                title: 'Qtd. usuários p/cada empresa filha',
                attribute: 'qtt_users_child',
                isActive: false
            });
        }

        this.columns.push({
            matColumnDef: 'active',
            title: 'Ativo',
            attribute: 'active',
            isActive: true
        });

        if (this.userAccess.id_company != null) {
            this.optionColumns.push({
                id: 'place',
                title: 'Lugares/Comodos',
                route: 'place'
            });
        }

        this.optionColumns.push({
            id: 'userAccessByCompany',
            title: 'Acessos de usuários',
            route: 'userAccess/byCompany'
        });
    }

    populateList() {
        if (this.userAccess.id_company != null) {
            this.service.getByParent(this.userAccess.id_company).subscribe((result: any[]) => {
                this.listOriginal = result.sort((a: any, b: any) => a.name.localeCompare(b.name));
                this.listOriginal.map(item => item.responsible = item.company.name);
            });
        } else if (this.userAccess.id_owner != null) {
            this.service.getByOwner(this.userAccess.id_owner).subscribe((result: any[]) => {
                this.listOriginal = result.filter(item => !item.id_company).sort((a: any, b: any) => a.name.localeCompare(b.name));
                this.listOriginal.map(item => item.responsible = item.owner.name);
            });
        }
    }

    deleteItem(item) {
        this.service.delete(item.id).subscribe(() => {
            this.generic.toastr.success('Exclusão executada com sucesso', this.pageTitle());
            this.populateList();
        });
    }

}
