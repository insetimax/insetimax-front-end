import { Component, Injector } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { GenericFormPage } from '../../../generic/generic-form.page';
import { ActivityCategoryService } from '../../../../services/activity-category/activity-category.service';

@Component({
    templateUrl: './activity-category-form.component.html',
    styleUrls: ['./activity-category-form.component.scss']
})
export class ActivityCategoryFormComponent extends GenericFormPage {

    constructor(
        private service: ActivityCategoryService,
        public injector: Injector
    ) {
        super(injector);
    }

    onInit(): void {
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public pageTitle(): string {
        return 'Atividade';
    }

    public pageMain(): string {
        return 'activityCategory';
    }

    public setForm(): void {
        this.form = new FormGroup({
            name: new FormControl('', [Validators.required]),
            active: new FormControl(true, [Validators.required])
        });
    }

    public getById(): void {
        this.service.getById(this.id).subscribe(result => {
            this.model = !!result ? result[0] : {};
            this.title += ` > ${this.model?.name}`;

            this.form.setValue({
                name: this.model.name,
                active: this.model.active
            });
        });
    }

    public save(): void {
        let body = {
            id_owner: this.userAccess.id_owner,
            name: this.form.get('name').value,
            active: this.form.get('active').value
        }

        if (!!this.isEdit) {
            this.service.put(this.id, body).subscribe(result => {
                this.generic.toastr.success(`Registro editado com sucesso`, this.pageTitle());
                this.generic.router.navigateByUrl(`/${this.pageMain()}`);
            });
        } else {
            this.service.post(body).subscribe(result => {
                this.generic.toastr.success(`Registro criada com sucesso`, this.pageTitle());
                this.generic.router.navigateByUrl(`/${this.pageMain()}`);
            });
        }
    }

    public patchImage(image: any): void {
    }

}
