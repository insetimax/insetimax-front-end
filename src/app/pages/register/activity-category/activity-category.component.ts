import { Component, Injector } from '@angular/core';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { GenericPage } from '../../generic/generic.page';
import { ActivityCategoryService } from '../../../services/activity-category/activity-category.service';

@Component({
    templateUrl: './activity-category.component.html',
    styleUrls: ['./activity-category.component.scss']
})
export class ActivityCategoryComponent extends GenericPage {

    constructor(
        private service: ActivityCategoryService,
        public injector: Injector
    ) {
        super(injector);
    }

    public onInit(): void {
    }

    public pageTitle(): string {
        return 'Atividades';
    }

    public pageForm(): string {
        return 'activityCategoryForm';
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REGISTER);
    }

    public setColumns() {
        this.columns = [{
            matColumnDef: 'id',
            title: 'ID',
            attribute: 'id',
            isActive: false
        }, {
            matColumnDef: 'name',
            title: 'Nome',
            attribute: 'name',
            isActive: false
        }, {
            matColumnDef: 'active',
            title: 'Ativo',
            attribute: 'active',
            isActive: true
        }];
    }

    populateList() {
        this.service.getByOwner(this.userAccess.id_owner).subscribe((result: any[]) => {
            this.listOriginal = result.sort((a: any, b: any) => a.name.localeCompare(b.name));
        });
    }

    deleteItem(item) {
        this.service.delete(item.id).subscribe(() => {
            this.generic.toastr.success('Exclusão executada com sucesso', this.pageTitle());
            this.populateList();
        });
    }

}