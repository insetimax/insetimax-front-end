import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from 'src/app/services/login/login.service';
import { UserService } from '../../../services/user/user.service';

@Component({
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

    private pageTitle: string = 'Alteração de senha';

    public idUser: number;
    public userCPF: number;
    public userName: string;

    public password: any = new FormControl('', [Validators.required]);
    public password2: any = new FormControl('', [Validators.required]);

    public hide: boolean = true;
    public hide2: boolean = true;

    constructor(
        private activatedRoute: ActivatedRoute,
        private loginService: LoginService,
        private userService: UserService,
        private toastr: ToastrService,
        private router: Router
    ) {
    }

    ngOnInit(): void {
        this.idUser = this.activatedRoute.snapshot.params?.idUser;
        if (!!this.idUser) {
            this.userService.getByIdChangePassword(this.idUser).subscribe((result: any) => {
                this.userCPF = result.cpf;
                this.userName = result.name;
            }, error => {
                this.toastr.error('Ocorreu um erro ao entrar na tela! Você está sendo redirecionado para o login', this.pageTitle);
                this.loginService.logout();
                // this.router.navigateByUrl('/login');
            });
        } else {
            this.toastr.error('Dados incorretos para entrar nessa tela! Você está sendo redirecionado para o login', this.pageTitle);
            this.loginService.logout();
            // this.router.navigateByUrl('/login');
        }
    }

    changePassword() {
        if (this.password.value === this.password2.value) {
            let body = {
                password: this.password.value,
                updatepass: false
            }

            this.userService.changePassword(this.idUser, body).subscribe(result => {
                this.toastr.success('Alteração realizada com sucesso! Você está sendo redirecionado para o login', this.pageTitle);
                this.loginService.logout();
                // this.router.navigateByUrl('/login');
            });
        } else {
            this.toastr.warning('Senhas informadas não conferem', this.pageTitle);
        }
    }

}