import { Component, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from 'src/app/services/login/login.service';
import { UserService } from '../../services/user/user.service';

@Component({
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {

    public cpf: any = new FormControl('', [Validators.required]);
    public password: any = new FormControl('', [Validators.required]);

    public hide: boolean = true;

    constructor(
        private userService: UserService,
        private loginService: LoginService,
        private toastr: ToastrService,
        private dialog: MatDialog
    ) {
    }

    forgotPassword() {
        const dialogRef = this.dialog.open(ForgotPasswordDialog, {
            data: {
                cpf: this.cpf.value
            }
        });

        dialogRef.afterClosed().subscribe((result: any) => {
            if (!!result && result.cpf != null && result.cpf != '') {
                const body = { cpf: result.cpf };
                this.userService.forgotPassword(body).subscribe(res => {
                    this.toastr.success('Nova senha gerada e e-mail com a senha enviado com sucesso', 'Esqueci minha senha?');
                    console.log('deu certo mizeravi');
                });
            }
        });
    }

    login() {
        this.loginService.validateLogin(this.cpf.value, this.password.value);
    }

}

@Component({
    selector: 'forgot-password-dialog',
    templateUrl: 'forgot-password-dialog.html',
})
export class ForgotPasswordDialog {

    public cpf: any = new FormControl('', [Validators.required]);

    constructor(
        public dialogRef: MatDialogRef<ForgotPasswordDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        if (!!data && !!data.cpf)
            this.cpf.setValue(data.cpf);
    }

    cancel() {
        this.dialogRef.close();
    }

    generate() {
        this.dialogRef.close({
            cpf: this.cpf.value
        });
    }

}