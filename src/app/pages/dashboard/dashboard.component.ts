import { Component, Injector } from '@angular/core';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { CompanyService } from 'src/app/services/company/company.service';
import { GenericDashboard } from '../generic/generic.dashboard';
import { UserAccessService } from 'src/app/services/user-access/user-access.service';
import * as _ from 'lodash';
import { CleanningService } from 'src/app/services/cleanning/cleannig.service';
import { TableColumn } from 'src/app/components/table/table.component';

@Component({
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends GenericDashboard {

    public qttCompany: number = 0;
    public qttCompanyLevel1: number = 0;
    public qttCompanyLevel2: number = 0;

    public qttAdmin1: number = 0;
    public qttCleaner1: number = 0;

    public qttAdmin2: number = 0;
    public qttCleaner2: number = 0;

    public classBox: string = '';

    private cleaningReportFilter: any = {};
    public cleaningReportListColumns: TableColumn[] = [];
    public cleaningReportList: any[] = [];

    constructor(
        public injector: Injector,
        private companyService: CompanyService,
        private cleaningService: CleanningService,
        private userAccessService: UserAccessService,
    ) {
        super(injector);
    }

    public onInit(): void {
        if (!!this.activatedRoute.snapshot.params?.byLogin)
            window.location.href = window.location.href.replace('/true', '');

        const now: any = new Date();
        const startTime = new Date(now - (7 * this.generic.milliSecondsInDay));
        const endTime = new Date();

        this.cleaningReportFilter = {
            idOwner: this.idOwner,
            idCompanyParent: null,
            startTime: '2021-05-14',//startTime.toISOString().split('T')[0],
            endTime: endTime.toISOString().split('T')[0],
            limit: 20
        }

        if (this.idCompany != null) {
            this.getDataByCompany();
        } else if (this.idOwner != null) {
            this.getDataByOwner();
        }

        this.getCleaningReport();
    }

    public pageTitle(): string {
        return 'Dashboard';
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.DASHBOARD);
    }

    private getDataByCompany() {
        this.classBox = 'col-md-4 col-sm-12 col-lg-4';
        this.cleaningReportFilter.idCompanyParent = this.idCompany;

        this.companyService.getByParent(this.idCompany).subscribe((result: any) => {
            this.qttCompany = result.filter((item: any) => !!item.active).length;
        });

        this.userAccessService.getByCompany(this.idCompany).subscribe((result1: any) => {
            const newResult1 = _.union(result1.map(function (item: any) {
                return {
                    id_user: item.id_user,
                    is_admin: item.is_admin,
                    is_cleaner: item.is_cleaner,
                    active: item.active
                }
            }));

            this.qttAdmin1 = newResult1.filter((item: any) => !!item.active && !!item.is_admin).length;
            this.qttCleaner1 = newResult1.filter((item: any) => !!item.active && !!item.is_cleaner).length;

            this.userAccessService.getByCompanyParent(this.idCompany).subscribe((result2: any) => {
                const newResult2 = _.union(result2.map(function (item: any) {
                    return {
                        id_user: item.id_user,
                        is_admin: item.is_admin,
                        is_cleaner: item.is_cleaner,
                        active: item.active
                    }
                }));

                this.qttAdmin2 = newResult2.filter((item: any) => !!item.active && !!item.is_admin).length;
                this.qttCleaner2 = newResult2.filter((item: any) => !!item.active && !!item.is_cleaner).length;
            });
        });
    }

    private getDataByOwner() {
        this.classBox = 'col-md-3 col-sm-12 col-lg-3';

        this.companyService.getByOwner(this.idOwner).subscribe((result: any) => {
            this.qttCompanyLevel1 = result.filter((item: any) => !!item.active && !item.id_company).length;
            this.qttCompanyLevel2 = result.filter((item: any) => !!item.active && !!item.id_company).length;
        });

        this.userAccessService.getByCompanyByOwner(this.idOwner).subscribe((result: any) => {
            const newResult = _.union(result.map(function (item: any) {
                return {
                    id_user: item.id_user,
                    is_admin: item.is_admin,
                    is_cleaner: item.is_cleaner,
                    active: item.active
                }
            }));

            this.qttAdmin2 = newResult.filter((item: any) => !!item.active && !!item.is_admin).length;
            this.qttCleaner2 = newResult.filter((item: any) => !!item.active && !!item.is_cleaner).length;
        });
    }

    private getCleaningReport() {
        this.cleaningReportListColumns = [{
            matColumnDef: 'user',
            title: 'Operador',
            attribute: 'user_name'
        }, {
            matColumnDef: 'company',
            title: 'Empresa',
            attribute: 'company_name'
        }, {
            matColumnDef: 'place',
            title: 'Lugar/Comodo',
            attribute: 'place_name'
        }, {
            matColumnDef: 'actCategory',
            title: 'Atividade',
            attribute: 'act_category_name'
        }, {
            matColumnDef: 'startTime',
            title: 'Data Início',
            attribute: 'start_time',
            dateFormat: 'DD/MM/YYYY HH:mm:ss'
        }, {
            matColumnDef: 'endTime',
            title: 'Data Fim',
            attribute: 'end_time',
            dateFormat: 'DD/MM/YYYY HH:mm:ss'
        }];

        this.cleaningService.cleaningReport(this.cleaningReportFilter).subscribe((result: any) => {
            this.cleaningReportList = result;
        });
    }

    goToPage(page) {
        this.generic.goToPage(page);
    }

}