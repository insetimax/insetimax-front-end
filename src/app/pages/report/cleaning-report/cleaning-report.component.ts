import { Component, Injector } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { ActivityCategoryService } from 'src/app/services/activity-category/activity-category.service';
import { CleanningService } from 'src/app/services/cleanning/cleannig.service';
import { CompanyService } from 'src/app/services/company/company.service';
import { PlaceService } from 'src/app/services/place/place.service';
import { UserService } from 'src/app/services/user/user.service';
import { GenericPage } from '../../generic/generic.page';

@Component({
    templateUrl: './cleaning-report.component.html',
    styleUrls: ['./cleaning-report.component.scss']
})
export class CleaningreportComponent extends GenericPage {

    filterPanelOpenState = true;

    userList: any[] = [];
    placeList: any[] = [];
    companyList: any[] = [];
    actCategoryList: any[] = [];
    periodList: any[] = [{
        id: 1, name: 'Manual', type: 'manual'
    }, {
        id: 2, name: 'Último dia', qtt: 1
    }, {
        id: 3, name: 'Últimos 3 dias', qtt: 3
    }, {
        id: 4, name: 'Últimos 5 dias', qtt: 5
    }, {
        id: 5, name: 'Últimos 7 dias', qtt: 7
    }, {
        id: 6, name: 'Últimos 10 dias', qtt: 10
    }];

    public form: FormGroup = new FormGroup({
        company: new FormControl(''),
        user: new FormControl(''),
        actCategory: new FormControl(''),
        place: new FormControl(''),
        period: new FormControl(1, [Validators.required]),
        startTime: new FormControl('', [Validators.required]),
        endTime: new FormControl('', [Validators.required]),
    });

    constructor(
        private actCategoryService: ActivityCategoryService,
        private companyService: CompanyService,
        private placeService: PlaceService,
        private userService: UserService,
        private service: CleanningService,
        public injector: Injector
    ) {
        super(injector);
    }

    public onInit(): void {
        this.actCategoryService.getByOwner(this.idOwner).subscribe((result: any) => {
            this.actCategoryList = result;
        });

        if (this.idCompany != null) {
            this.companyService.getByParent(this.idCompany).subscribe((result: any) => {
                this.companyList = result;
            });

            this.userService.getAllUsersOfCompany(this.user.id).subscribe((result: any) => {
                this.userList = result;
            });
        } else if (this.idOwner != null) {
            this.companyService.getByOwner(this.idOwner).subscribe((result: any) => {
                this.companyList = result;
            });

            this.userService.getAllUsersOfOwner(this.user.id).subscribe((result: any) => {
                this.userList = result;
            });
        }
    }

    public pageTitle(): string {
        return 'Operações';
    }

    public pageForm(): string {
        return '';
    }

    public setSidebar(): void {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REPORT);
    }

    public setColumns() {
        this.columns = [{
            matColumnDef: 'user',
            title: 'Operador',
            attribute: 'user_name'
        }, {
            matColumnDef: 'company',
            title: 'Empresa',
            attribute: 'company_name'
        }, {
            matColumnDef: 'place',
            title: 'Lugar/Comodo',
            attribute: 'place_name'
        }, {
            matColumnDef: 'actCategory',
            title: 'Atividade',
            attribute: 'act_category_name'
        }, {
            matColumnDef: 'startTime',
            title: 'Data Início',
            attribute: 'start_time',
            dateFormat: 'DD/MM/YYYY HH:mm:ss'
        }, {
            matColumnDef: 'endTime',
            title: 'Data Fim',
            attribute: 'end_time',
            dateFormat: 'DD/MM/YYYY HH:mm:ss'/*
        }, {
            matColumnDef: 'closedAuto',
            title: 'Fechado automaticamente?',
            attribute: 'closed_automatically'
        }, {
            matColumnDef: 'assocPlaceActCategNotFound',
            title: 'Assoc. Lugar x Categ. Ativ.',
            attribute: 'assoc_place_act_categ_not_found'*/
        }];
    }

    populateList() {
        this.listOriginal = [];
    }

    deleteItem(item) {
    }

    changePeriod() {
        const periodId = this.form.get('period').value;
        let startTime: any = new Date();
        let endTime = new Date();

        if (periodId == 1) {
            startTime = null;
            endTime = null;
        } else {
            const period = this.periodList.find((item: any) => item.id == periodId);
            startTime = new Date(startTime - (period.qtt * this.generic.milliSecondsInDay))
        }

        this.form.patchValue({
            startTime: startTime,
            endTime: endTime,
        });
    }

    changeCompany() {
        this.placeList = [];
        if (!!this.form.get('company').value) {
            this.placeService.getByCompany(this.form.get('company').value).subscribe((result: any) => {
                this.placeList = result;
            });
        }
    }

    applyFilter() {
        if (this.form.status == 'VALID') {
            let filters = {
                idOwner: this.idOwner,
                idCompanyParent: this.idCompany,
                idUser: this.form.get('user').value,
                idCompany: this.form.get('company').value,
                idPlace: this.form.get('place').value,
                idActCategory: this.form.get('actCategory').value,
                startTime: (this.form.get('startTime').value).toISOString().split('T')[0],
                endTime: (this.form.get('endTime').value).toISOString().split('T')[0],
                limit: null
            }

            this.service.cleaningReport(filters).subscribe((result: any) => {
                this.listOriginal = result;
            });
        }
    }

}
