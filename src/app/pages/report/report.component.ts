import { Component, OnInit } from '@angular/core';
import { SessionStorageEnum } from 'src/app/enums/session-storage.enum';
import { SidebarEnum } from 'src/app/enums/sidebar.enum';
import { GenericService } from 'src/app/services/generic/generic.service';

@Component({
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

    public pageList = [{
        id: 'cleanning',
        name: 'Operações',
        route: 'cleaningReport',
        role: 'all'
    }];

    constructor(
        private generic: GenericService
    ) {
        this.generic.sessionStorage.store(SessionStorageEnum.SIDEBAR_MENU, SidebarEnum.REPORT);
    }

    ngOnInit(): void {
        let userAccess = this.generic.getUserAccess();
        if (!!userAccess?.role?.is_admin) {
            if (userAccess.id_company != null)
                this.pageList = this.pageList.filter(item => item.role == 'all')
        } else {
            this.pageList = [];
        }
    }

    goToPage(page) {
        this.generic.goToPage(page);
    }

}