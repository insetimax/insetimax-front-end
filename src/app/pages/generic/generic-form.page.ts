import { Injectable, Injector, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { GenericService } from "src/app/services/generic/generic.service";

@Injectable({
    providedIn: 'root'
})
export abstract class GenericFormPage implements OnInit {

    public idCompany: number;
    public idOwner: number;
    public id: number;

    public isEdit: boolean = false;
    public title: string = '';
    public userAccess: any = {};
    public model: any = {};

    public form: FormGroup;

    public generic: GenericService;
    public activatedRoute: ActivatedRoute;

    public imageFieldName: string = 'image';

    constructor(
        public injector: Injector
    ) {
        this.generic = this.injector.get(GenericService);
        this.activatedRoute = this.injector.get(ActivatedRoute);
        this.userAccess = this.generic.getUserAccess();
        this.setSidebar();
        this.setForm();
    }

    ngOnInit(): void {
        this.idCompany = this.activatedRoute.snapshot.params?.idCompany;
        this.idOwner = this.activatedRoute.snapshot.params?.idOwner;
        this.id = this.activatedRoute.snapshot.params?.id;

        if (!!this.id) {
            this.isEdit = true;
            this.title = `Edição > ${this.pageTitle()}`;
            this.getById();
        } else {
            this.isEdit = false;
            this.title = `Cadastro > ${this.pageTitle()}`;
        }

        this.onInit();
    }

    public abstract onInit(): void;

    public abstract setSidebar(): void;

    public abstract pageTitle(): string;

    public abstract pageMain(): string;

    public abstract setForm(): void;

    public abstract getById(): void;

    public abstract save(): void;

    public abstract patchImage(image: any): void;

    public cancel() {
        this.generic.router.navigateByUrl(`/${this.pageMain()}`);
    }

    cleanImage() {
        let element: any = document.getElementById(this.imageFieldName);
        element.value = '';
        this.patchImage('');
    }

    handleFileSelect(evt) {
        let files = evt.target.files;
        let file = files[0];
        if (files && file) {
            if (file.type.indexOf('image/png') >= 0 || file.type.indexOf('image/jpeg') >= 0) {
                let reader = new FileReader();
                reader.onload = this._handleReaderLoaded.bind(this);
                reader.readAsBinaryString(file);
            } else {
                this.generic.toastr.error('Formato inválido', 'Upload de arquivo');
                this.cleanImage();
            }
        }
    }

    private _handleReaderLoaded(readerEvt) {
        let binaryString = readerEvt.target.result;
        let base64String = btoa(binaryString);
        this.compressImage(base64String, 200, 200).then((compressed: any) => {
            this.patchImage(compressed.replace('data:image/png;base64,', ''));
        });
    }

    private compressImage(src, newX, newY) {
        return new Promise((res, rej) => {
            const img = new Image();
            img.src = `data:image/png;base64,${src}`;
            img.onload = () => {
                const elem = document.createElement('canvas');
                elem.width = newX;
                elem.height = newY;
                const ctx = elem.getContext('2d');
                ctx.drawImage(img, 0, 0, newX, newY);
                const data = ctx.canvas.toDataURL();
                res(data);
            }
            img.onerror = error => rej(error);
        });
    }

}
