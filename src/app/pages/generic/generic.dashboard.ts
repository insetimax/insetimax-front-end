import { Injectable, Injector, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { OptionColumn, TableColumn } from "src/app/components/table/table.component";
import { GenericService } from "src/app/services/generic/generic.service";

@Injectable({
    providedIn: 'root'
})
export abstract class GenericDashboard implements OnInit {

    public isCompany: boolean = false;
    public idCompany: number;
    public idOwner: number;

    public userAccess;
    public user;

    public generic: GenericService;
    public activatedRoute: ActivatedRoute;

    constructor(
        public injector: Injector
    ) {
        this.generic = this.injector.get(GenericService);
        this.activatedRoute = this.injector.get(ActivatedRoute);

        this.user = this.generic.getUser();
        this.userAccess = this.generic.getUserAccess();

        this.idCompany = this.activatedRoute.snapshot.params?.idCompany;
        this.idOwner = this.activatedRoute.snapshot.params?.idOwner;

        if (this.idOwner == null)
            this.idOwner = this.userAccess.id_owner;

        if (this.idOwner == null) {
            this.idOwner = this.userAccess.company.id_owner;

            if (!this.idCompany)
                this.idCompany = this.userAccess.id_company;
        }

        if (this.idCompany != null)
            this.isCompany = true;

        this.setSidebar();
    }

    ngOnInit(): void {
        this.onInit();
    }

    public abstract onInit(): void;

    public abstract pageTitle(): string;

    public abstract setSidebar(): void;

}
