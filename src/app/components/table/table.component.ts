import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { GenericService } from 'src/app/services/generic/generic.service';
import { SweetAlertService } from 'src/app/services/sweet-alert/sweet-alert.service';
import * as moment from 'moment';

export interface TableColumn {
    matColumnDef: string;
    title: string;
    attribute: string;
    attributeElse?: string;
    isActive?: boolean;
    isList?: boolean;
    mask?: string;
    dateFormat?: string;
}

export interface OptionColumn {
    id: string;
    title: string;
    route?: string;
    routeManual?: boolean;
    event?: boolean;
    comparator?: {
        value: string;
        attribute: string;
    }
}

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnChanges, AfterViewInit {

    @Input('columns') public inputColumns: TableColumn[] = [];
    @Input('fieldToShowMessage') public inputFieldToShowMessage: string = 'name';
    @Input('listOriginal') public inputListOriginal: any[] = [];
    @Input('optionColumns') public inputOptionColumns: OptionColumn[] = [];
    @Input('pageForm') public inputPageForm: string = '';

    @Input('showActions') public inputShowActions: boolean = true;
    @Input('showDelete') public inputShowDelete: boolean = true;
    @Input('showEdit') public inputShowEdit: boolean = true;

    @Input('hasActive') public inputHasActive: boolean = true;

    @Output('deleteItem') public outputDeleteItem: EventEmitter<any> = new EventEmitter();
    @Output('clickOption') public outputClickOption: EventEmitter<any> = new EventEmitter();

    @ViewChild(MatPaginator) paginator: MatPaginator;

    public displayedColumns: any[] = [];
    public list: MatTableDataSource<any>;
    public filterActive: string = 'active';

    constructor(
        private generic: GenericService,
        private sweetAlert: SweetAlertService
    ) {
    }

    ngOnInit(): void {
        this.applyFilterActiveCurrent();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.hasOwnProperty('inputListOriginal')) {
            if (changes.inputListOriginal.currentValue != changes.inputListOriginal.previousValue)
                this.applyFilterActiveCurrent();
        }

        if (changes.hasOwnProperty('inputColumns')) {
            if (changes.inputColumns.currentValue != changes.inputColumns.previousValue) {
                this.displayedColumns = [...this.inputColumns.map(item => item.matColumnDef)];
                if (!!this.inputShowActions)
                    this.displayedColumns.push('action');
            }
        }
    }

    ngAfterViewInit(): void {
        if (!!this.list)
            this.list.paginator = this.paginator;
    }

    protected setPaginator() {
        if (this.list.paginator) {
            this.list.paginator.firstPage();
        }
    }

    public applyFilter(filter: any) {
        const filterValue = (filter.target as HTMLInputElement).value;
        this.list.filter = filterValue.trim().toLowerCase();
        this.setPaginator();
    }

    public applyFilterActive() {
        if (!this.inputListOriginal)
            return;

        if (!this.inputHasActive)
            this.filterActive = 'all';

        if (this.filterActive == 'active') {
            this.list = new MatTableDataSource(this.inputListOriginal.filter(item => !item.active));
            this.filterActive = 'inactive';
        } else if (this.filterActive == 'inactive') {
            this.list = new MatTableDataSource(this.inputListOriginal);
            this.filterActive = 'all';
        } else if (this.filterActive == 'all') {
            this.list = new MatTableDataSource(this.inputListOriginal.filter(item => !!item.active));
            this.filterActive = 'active';
        }

        this.list.paginator = this.paginator;
        this.setPaginator();
    }

    public applyFilterActiveCurrent() {
        if (!this.inputListOriginal)
            return;

        if (!this.inputHasActive)
            this.filterActive = 'all';

        if (this.filterActive == 'active') {
            this.list = new MatTableDataSource(this.inputListOriginal.filter(item => !!item.active));
        } else if (this.filterActive == 'inactive') {
            this.list = new MatTableDataSource(this.inputListOriginal.filter(item => !item.active));
        } else if (this.filterActive == 'all') {
            this.list = new MatTableDataSource(this.inputListOriginal);
        }
        this.list.paginator = this.paginator;
        this.setPaginator();
    }

    public getFilterActiveIcon() {
        if (this.filterActive == 'active')
            return 'check_box';
        else if (this.filterActive == 'inactive')
            return 'check_box_outline_blank';
        else if (this.filterActive == 'all')
            return 'indeterminate_check_box';
    }

    public getItemValue(item, col) {
        let value = '';
        if (!col.isList) {
            let colList = col.attribute.split('.');
            value = item[colList[0]];
            for (let row = 1; row < colList.length; row++) {
                if (!!value)
                    value = value[colList[row]];
            }

            if (!value && !!col.attributeElse) {
                let colListElse = col.attributeElse.split('.');
                value = item[colListElse[0]];
                for (let row = 1; row < colListElse.length; row++) {
                    if (!!value)
                        value = value[colListElse[row]];
                }
            }

            if (!!value && !!col.dateFormat) {
                value = moment(value).format(col.dateFormat);
            }
        } else {
            let colList = item[col.attribute].split(',');
            colList.sort((a, b) => a.localeCompare(b));
            for (let row of colList) {
                value += `${!!value ? ' &#13; <br/> \n' : ''} ${row}`;
            }
        }
        return value;
    }

    public clickOption(opt: OptionColumn, item) {
        if (!!opt.route && !opt.event) {
            if (!!opt.routeManual)
                this.generic.goToPage(`/${opt.route}`);
            else
                this.generic.goToPage(`/${opt.route}/${item.id}`);
        } else if (!opt.route && !!opt.event) {
            this.outputClickOption.emit({
                opt: opt,
                item: item
            });
        }
    }

    public showOptionColumns(item, opt) {
        if (!!this.inputOptionColumns && this.inputOptionColumns.length > 0) {
            if (!!opt.comparator) {
                let value = opt.comparator.value;
                let valueAttribute = item[opt.comparator.attribute];
                if (valueAttribute != null && value != null) {
                    if (valueAttribute.indexOf(value) >= 0)
                        return true;
                } else {
                    return false;
                }

            } else {
                return true;
            }
        }
        return false;
    }

    public edit(id) {
        this.generic.goToPage(`/${this.inputPageForm}/${id}`);
    }

    public deleteItem(item) {
        let fieldList = this.inputFieldToShowMessage.split(',');
        let col = {};
        let count = 0;
        let label = '';

        for (let row of fieldList) {
            col = { attribute: row.trim() };

            if (count > 1)
                label += ' | ';

            label += this.getItemValue(item, col);

            count++;
        }

        this.sweetAlert.alertConfirm(
            'Exclusão',
            `Tem certeza que deseja excluir o item "${label}"?`
        ).then(retSweet => {
            if (!!this.sweetAlert.ok(retSweet)) {
                this.outputDeleteItem.emit(item);
            }
        });
    }

}
