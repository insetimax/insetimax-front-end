import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { GenericService } from 'src/app/services/generic/generic.service';

export interface PageHeaderOptionButton {
    id: string;
    title: string;
    icon: string;
    route?: string;
    event?: boolean;
}

@Component({
    selector: 'app-page-header',
    templateUrl: './page-header.component.html',
    styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnChanges {

    @Input('title') public inputTitle: string = '';
    @Input('routeAdd') public inputRouteAdd: string = '';
    @Input('routeBack') public inputRouteBack: string = '';
    @Input('optionButton') public inputOptionButton?: PageHeaderOptionButton[] = [];

    @Output('clickOption') public outputClickOption: EventEmitter<any> = new EventEmitter();

    constructor(
        private generic: GenericService
    ) {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.hasOwnProperty('inputOptionButton')) {
            console.log('teve alteração');
            console.log(this.inputOptionButton);
            console.log(changes.inputOptionButton);
        }
    }

    clickOption(opt) {
        if (!!opt.route && !opt.event) {
            this.goToPage(opt.route);
        } else if (!opt.route && !!opt.event) {
            this.outputClickOption.emit(opt);
        }
    }

    goToPage(page) {
        this.generic.goToPage(page);
    }

}
