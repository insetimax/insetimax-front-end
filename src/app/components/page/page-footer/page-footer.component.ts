import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { SweetAlertService } from 'src/app/services/sweet-alert/sweet-alert.service';

@Component({
    selector: 'app-page-footer',
    templateUrl: './page-footer.component.html',
    styleUrls: ['./page-footer.component.scss']
})
export class PageFooterComponent implements OnInit {

    @Input('form') public inputForm: FormGroup;
    @Output('save') public outputSave: EventEmitter<any> = new EventEmitter();
    @Output('cancel') public outputCancel: EventEmitter<any> = new EventEmitter();

    constructor(
        private sweetAlert: SweetAlertService
    ) { }

    ngOnInit(): void {
    }

    save() {
        if (!!this.inputForm.valid)
            this.outputSave.emit();
    }

    cancel() {
        this.sweetAlert.alertConfirm('Cancelar', 'Tem certeza que deseja cancelar essa operação?').then(retSweet => {
            if (!!this.sweetAlert.ok(retSweet))
                this.outputCancel.emit();
        });
    }

}
